﻿using AutoMapper;
using CrowdFunding.Models.DBModels;
using CrowdFunding.Models.Repositories;
using CrowdFunding.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace CrowdFunding
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            //Creating Default Roles
            RoleRepository RoleRepo = new RoleRepository();
            RoleRepo.CreateDefaultRoles();
            //End Creating Default Roles

            //AutoMapper Settings//
            var config = new MapperConfiguration(cfg => {
                cfg.CreateMap<Lender_VM, User>();
                cfg.CreateMap<Borrower_VM, User>();
            });
            //End AutoMapper Settings//
        }
    }
}
