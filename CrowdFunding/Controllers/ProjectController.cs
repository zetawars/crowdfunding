﻿using CrowdFunding.Models.DBModels;
using CrowdFunding.Models.Repositories;
using CrowdFunding.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CrowdFunding.Controllers
{
    public class ProjectController : PanelController
    {
        public ProjectRepository ProjectRepo { get; set; }

        public ProjectController()
        {
            this.ProjectRepo = new ProjectRepository();
        }



        public ActionResult Projects()
        {
            if (user.Role.ID == (int)DefaultRole.Borrower)
            {
                return View(ProjectRepo.GetList(user.ID));
            }
            else
            {
                return View(ProjectRepo.GetList());
            }
        }

        
        [HttpGet]
        public ActionResult AddProject(int ID = 0)
        {
            Project_VM vm = new Project_VM();
            vm.Tags = ProjectRepo.GetAllTags();
            
            if (ID == 0)
            {
                return View(vm);
            }
            else
            {
                vm.project = ProjectRepo.Get(ID);
                vm.SelectedTags = ProjectRepo.GetProjectTags(ID).Select(x => x.ID.ToString()).ToList();
                vm.ProjectImages = ProjectRepo.GetProjectImages(ID);
                return View(vm);
            }
        }
        [HttpPost]
        public ActionResult AddProject(Project_VM vm)
        {
            vm.project.UserID = user.ID;
            if (vm.ImageFiles != null)
            {
                foreach (var ImageFile in vm.ImageFiles)
                {
                    if (ImageFile != null)
                    {

                        string fn = Path.GetFileName(ImageFile.FileName);
                        string fileExtension = fn.Remove(0, fn.IndexOf('.') + 1);
                        fn = $"Project_{user.ID}{(new Random().NextDouble()) * 1000}." + fileExtension;
                        string SaveLocation = "~/Images/Projects/";
                        vm.ProjectImages.Add(Path.Combine(SaveLocation, fn));
                        string FilePath = Server.MapPath(SaveLocation);
                        ImageFile.SaveAs(Path.Combine(FilePath, fn));
                    }

                }
            }
            if (vm.project.ID == 0)
            {
                ProjectRepo.Insert(vm.project);
                if(vm.ProjectImages != null &&  vm.ProjectImages.Count > 0)
                {
                    ProjectRepo.InsertImages(vm.project.ID, vm.ProjectImages);
                }
                if(vm.SelectedTags != null && vm.SelectedTags.Count > 0)
                {
                    ProjectRepo.InsertTags(vm.project.ID, vm.SelectedTags);
                }
                Notify("Success", "Successfully Added", "your project has been Added successfully", false, false, true);
            }
            else
            {
                ProjectRepo.Update(vm.project);
                ProjectRepo.UpdateImages(vm.project.ID, vm.ProjectImages);
                ProjectRepo.UpdateTags(vm.project.ID, vm.SelectedTags);

                Notify("Success", "Successfully Updated", "your project has been updated successfully", false, false, true);


            }

            return RedirectToAction("AddProject", "Project", new { ID = vm.project.ID});

        }
        public ActionResult DeleteProject(int ID)
        {

            ProjectRepo.DeleteProject(ID);
            Notify("Success", "Successfully Added", "your project has been Added successfully", false, false, true);
           return RedirectToAction("AddProject", "Project", new { ID });

        }

        public JsonResult ConfirmProject(int ProjectID)
        {
            return Json(ProjectRepo.ConfirmProject(ProjectID, user.ID));
        }
        public JsonResult CancelProject(int ProjectID)
        {
            return  Json(ProjectRepo.CancelProject(ProjectID, user.ID));
        }


        public ActionResult ProjectFunding()
        {
            List<Models.ViewModels.ProjectFunding> InvestmentList = ProjectRepo.GetProjectFundings(user.ID, user.Role.ID);
            return View(InvestmentList);
        }

    }
}