﻿using CrowdFunding.Models.DBModels;
using CrowdFunding.Models.Filters;
using CrowdFunding.Models.Other;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CrowdFunding.Controllers
{
    [RequireSession]
    [CredentialsVerified]
    public class PanelController : BaseController
    {
        public SessionUser user { get {  return Session["User"] as SessionUser;} }
    }
}