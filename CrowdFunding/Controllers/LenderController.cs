﻿using CrowdFunding.Models.Filters;
using CrowdFunding.Models.Repositories;
using CrowdFunding.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CrowdFunding.Controllers
{
    public class LenderController : PanelController
    {

        public UserRepository UserRepo { get; set; }
        public LenderController()
        {
            this.UserRepo = new UserRepository();
        }
        [Role("Lender")]
        public ActionResult Dashboard()
        {
            return View(GetDashboardVM());
        }

        public Dashboard_VM GetDashboardVM()
        {
            Dashboard_VM vm = new Dashboard_VM();
            vm.LenderList = UserRepo.GetLenderList();
            vm.BorrowerList = UserRepo.GetBorrowerList();
            vm.Lenders = vm.LenderList.Count;
            vm.Borrowers = vm.BorrowerList.Count;
            return vm;
        }



        public ActionResult Portfolio()
        {
            return View();
        }

        public ActionResult Profile()
        {
            return View();
        }


        [HttpGet]
        public ActionResult Wallet()
        {
            MiniORM ORM = new MiniORM();
            ViewBag.Amount = ORM.GetScaler("SELECT AMOUNT FROM [Users].[UserAccount] Where UserID = @ID", new { user.ID });

            return View();
        }

        [HttpPost]
        public ActionResult Wallet(string k)
        {
            MiniORM ORM = new MiniORM();
           double.TryParse(Request["Amount"], out double Amount);

            string query = @"
IF Not Exists(SELECT TOP 1 * FROM Users.[UserAccount] where UserID = @ID)
    BEGIN
        INSERT INTO Users.[UserAccount] (UserID, Amount ) VALUES (@ID, @Amount)
    END
ELSE
    BEGIN 
        UPDATE Users.[UserAccount] SET Amount += @Amount where UserID = @ID
    END
";

            ORM.ExecuteQuery(query, new { user.ID, Amount });
            return RedirectToAction("Wallet", "Lender");
        }
    }
}