﻿using CrowdFunding.Hubs;
using CrowdFunding.Models.Other;
using Microsoft.AspNet.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;


[assembly: log4net.Config.XmlConfigurator(Watch = true)]
namespace CrowdFunding.Controllers
{
    public class BaseController : Controller
    {
        protected IHubContext Context { get; set; }
        private readonly log4net.ILog Logger;
        public BaseController()
        {
            this.Context = GlobalHost.ConnectionManager.GetHubContext<Notification>();
            this.Logger = log4net.LogManager.GetLogger("BaseController");
        }


        [NonAction]
        public virtual void ErrorLog(string text)
        {
            Logger.Error(Request.UserHostAddress + " " + text);

        }

        [NonAction]
        public void DebugLog(string text)
        {
            Logger.Debug(Request.UserHostAddress + " " + text);
        }

        [NonAction]
        public void InfoLog(string text)
        {
            Logger.Info(Request.UserHostAddress + " " + text);
        }

        [NonAction]
        public void WarningLog(string text)
        {
            Logger.Warn(Request.UserHostAddress + " " + text);
        }


        public ActionResult DefaultSettings()
        {
            return RedirectToAction("Login", "Account");
        }


        public void Notify(string Type, string Title, string Description, bool IsAjaxMessage = true, bool IsViewMessage = true, bool IsRedirectMessage = false)
        {
            Notify(new NotificationMessage
            {
                MessageType = Type,
                Title = Title,
                Description = Description,
                IsAjaxMessage = IsAjaxMessage,
                IsViewMessage = IsViewMessage,
                IsRedirectMessage = IsRedirectMessage
            });
        }



        public void NotifyView(string Type, string Title, string Description)
        {
            Notify(new NotificationMessage
            {
                MessageType = Type,
                Title = Title,
                Description = Description,
                IsAjaxMessage = false,
                IsViewMessage = true,
                IsRedirectMessage = false
            });
        }

        public void NotifyRedirect(string Type, string Title, string Description, bool IsAjaxMessage = true, bool IsViewMessage = true, bool IsRedirectMessage = false)
        {
            Notify(new NotificationMessage
            {
                MessageType = Type,
                Title = Title,
                Description = Description,
                IsAjaxMessage = IsAjaxMessage,
                IsViewMessage = IsViewMessage,
                IsRedirectMessage = IsRedirectMessage
            });
        }


        public void NotifyAjax(string Type, string Title, string Description, bool IsAjaxMessage = true, bool IsViewMessage = true, bool IsRedirectMessage = false)
        {
            Notify(new NotificationMessage
            {
                MessageType = Type,
                Title = Title,
                Description = Description,
                IsAjaxMessage = IsAjaxMessage,
                IsViewMessage = IsViewMessage,
                IsRedirectMessage = IsRedirectMessage
            });
        }

        public virtual void Notify(NotificationMessage message)
        {
            if (message.IsAjaxMessage)
            {
                Context.Clients.Group(Request.UserHostAddress).Notify(message);
            }
            if (message.IsRedirectMessage)
            {
                TempData["NotificationMessage"] = message;
            }
            if (message.IsViewMessage)
            {
                ViewBag.NotificationMessage = message;
            }
        }

        public new RedirectToRouteResult RedirectToAction(string action, string controller)
        {
            return base.RedirectToAction(action, controller);
        }


    }
}