﻿using CrowdFunding.Models.Other;
using CrowdFunding.Models.Repositories;
using CrowdFunding.Models.ViewModels;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CrowdFunding.Controllers
{
    public class HomeController : BaseController
    {
        public ProjectRepository ProjectRepo { get; set; }
        public UserRepository UserRepo { get; set; }

        public HomeController()
        {
            this.ProjectRepo = new ProjectRepository();
            this.UserRepo = new UserRepository();
        }
        // GET: Home
        public ActionResult Index()
        {
            Index_VM vm = new Index_VM();
            vm.FeaturedProjects = ProjectRepo.GetFeaturedProjects();
            vm.PopularProjects = ProjectRepo.GetPopularProjects();
            return View(vm);
        }


        [HttpGet]
        public ActionResult ProjectDetail(int ProjectID)
        {
            int ID = ProjectID;
            Project_VM vm = new Project_VM();
            vm.project = ProjectRepo.Get(ID);
            vm.ProjectImages = ProjectRepo.GetImages(ID);
            vm.Investors = ProjectRepo.GetInvestors(ID);
            vm.Backers = vm.Investors.Count;
            vm.Pledged = ProjectRepo.GetPlegedAmount(ID);
            vm.Tags = ProjectRepo.GetProjectTags(ID);
            //vm.ProjectOwner = ProjectRepo.GetOwner(ID);
            SessionUser user = Session["User"] as SessionUser;
            if (user != null)
            {
                ViewBag.UserWallet = UserRepo.GetWalletAmount(user.ID).ToString();
            }
            else
            {
                ViewBag.UserWallet = "";

            }

            return View(vm);
        }

        [HttpPost]
        public ActionResult ProjectDetail(double InvestAmount, int ProjectID)
        {
            SessionUser user = Session["User"] as SessionUser;
            if (user != null)
            {
                double UserWallet = UserRepo.GetWalletAmount(user.ID);
                if (UserWallet != 0)
                {
                    if (InvestAmount <= UserWallet)
                    {
                        MiniORM ORM = new MiniORM();
                        string query = @"
INSERT INTO Projects.Project_Transactions (ProjectID, LenderiD, TransferedAmount) VALUES  (@ProjectID,@ID, @InvestAmount);
UPDATE Users.[UserAccount] SET Amount -= @InvestAmount where UserID = @ID;
";
                        ORM.ExecuteTransactionQuery(query, new { user.ID, ProjectID, InvestAmount });

                    }

                }
            }
            return RedirectToAction($"ProjectDetail", "Home", new { ProjectID });
        }
    }
}