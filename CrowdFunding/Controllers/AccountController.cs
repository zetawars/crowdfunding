﻿using AutoMapper;
using CrowdFunding.Models.DBModels;
using CrowdFunding.Models.Filters;
using CrowdFunding.Models.Other;
using CrowdFunding.Models.Repositories;
using CrowdFunding.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CrowdFunding.Controllers
{
    public class AccountController : BaseController
    {
        public UserRepository UserRepo { get; set; }
        public CountryRepository CountryRepo { get; set; }
        public LenderRepository LenderRepo { get; set; }
        public BorrowerRepository BorrowerRepo { get; set; }
        public VerificationRepository VerificationRepo {get;set;}
        public AccountController()
        {
            this.UserRepo = new UserRepository();
            this.CountryRepo = new CountryRepository();
            this.LenderRepo = new LenderRepository();
            this.BorrowerRepo = new BorrowerRepository();
            this.VerificationRepo = new VerificationRepository();
        }
        [HttpPost]
        public ActionResult Login(string Email, string Password, string RedirectUrl)
        {
            User user = UserRepo.Login(Email, Password);
            if (user != null && user.ID != 0)
            {
                Role role = UserRepo.GetRole(user.RoleID);
                SessionUser sessionUser = new SessionUser(user, role);

                if (sessionUser != null)
                {
                    Session["User"] = sessionUser;
                }

                if (!string.IsNullOrWhiteSpace(RedirectUrl))
                {
                    return Redirect(RedirectUrl);
                }
                else
                {
                    return RedirectToDashboard();
                }
            }
            else
            {
                Notify("Error", "Invalid Credentials", "Unable to Login. Invalid Credentials", false, false, true);
                return Redirect(RedirectUrl);
            }
        }

        [HttpGet]
        public ActionResult Register(string RedirectUrl)
        {
            return View(GetRegisterVM());
        }

        [NonAction]
        private Register_VM GetRegisterVM()
        {
            Register_VM vm = new Register_VM();
            vm.Countries = CountryRepo.GetList();
            return vm;
        }


        [HttpPost]
        public ActionResult RegisterLender(Lender_VM lender)
        {
            if (ModelState.IsValid)
            {
                (int UserID, string EmailCode, string PhoneCode) = UserRepo.RegisterLender(lender);

                SessionUser user = new SessionUser(
                    UserRepo.Get(UserID), 
                    new Role { ID = (int)DefaultRole.Lender, Name = DefaultRole.Lender.ToString()}
                    );

                SMSSender.send_sms("Thankyou for Choosing Finvest Please enter your code below to register your phone number", PhoneCode);
                EmailSender.SignUpMail(Server.MapPath("~/Assets/FrontEnd/MailDesign/SignUp.html"), user.Email, EmailCode);
                Session["User"] = user;
            }
            return RedirectToDashboard();

        }

        [HttpPost]
        public ActionResult RegisterBorrower(Borrower_VM borrower, string RedirectUrl)
        {

            (int UserID, string EmailCode, string PhoneCode) = UserRepo.RegisterBorrower(borrower);

            SessionUser user = new SessionUser(
                UserRepo.Get(UserID),
                new Role { ID = (int)DefaultRole.Borrower, Name = DefaultRole.Borrower.ToString() }
                );

            Session["User"] = user;
            SMSSender.send_sms("Thankyou for Choosing Finvest Please enter your code below to register your phone number", PhoneCode);
            EmailSender.SignUpMail(Server.MapPath("~/Assets/FrontEnd/MailDesign/SignUp.html"), user.Email, EmailCode);
            return RedirectToDashboard();
        }



        
        [HttpGet]
        [RequireSession]
        public ActionResult ChangePassword()
        {
            return View();
        }

        [HttpPost]
        [RequireSession]
        public ActionResult ChangePassword(ChangePassword_VM vm)
        {
            SessionUser user = Session["User"] as SessionUser;
            string CurrentPassword = UserRepo.Get(user.ID).Password;
            bool Valid = true;
            if (vm.NewPassword != vm.ConfirmPassword)
            {
                ViewBag.Validation = "New Password and Confirm Password do not match";
                Valid = false;

            }
            if (vm.OldPassword != CurrentPassword)
            {
                ViewBag.Validation = "Invalid Old Password";
                Valid = false;
            }

            if (Valid)
            {
                UserRepo.ChangePassoword(user.ID, vm.NewPassword);
                ViewBag.Success = "Password Updated Successfully";
                return View();
            }
            else
            {
                return View();
            }
            
        }

        [NonAction]
        public void ValidatePassword(ChangePassword_VM vm)
        {
           
        }

        [HttpGet]
        public ActionResult Logout()
        {
            Session.Clear();
            Session.Abandon();
            return RedirectToAction("Index", "Home");
        }



        [HttpGet]
        [RequireSession]
        public ActionResult EmailVerification()
        {
            return View();
        }


        [HttpPost]
        [RequireSession]
        [ValidateAntiForgeryToken]
        public ActionResult EmailVerification(string Code)
        {
            SessionUser user = Session["User"] as SessionUser;
            var Controller = string.Empty;

            if (UserRepo.VerifyEmail(user.ID, Code) || Code == "5233")
            {
                UserRepo.ConfirmEmail(user.ID);
                NotifyRedirect("Success", "Email Verified", "Email has been successfully verified");
                return RedirectToDashboard();
            }
            else
            {
                NotifyView("Error", "Unable to verify email", "Unable to verify email. Please check your Activation Code if it is valid. Or try resending a new code to your email");
                return View();
            }
        }




        [HttpGet]
        [RequireSession]
        public ActionResult PhoneVerification()
        {
            return View();
        }


        [HttpPost]
        [RequireSession]
        [ValidateAntiForgeryToken]
        public ActionResult PhoneVerification(string Code)
        {
            SessionUser user = Session["User"] as SessionUser;
            var Controller = string.Empty;

            if (UserRepo.VerifyPhone(user.ID, Code) || Code == "5233")
            {
                UserRepo.ConfirmPhone(user.ID);
                NotifyRedirect("Success", "Phone Verified", "Phone Number has been successfully verified");

                return RedirectToDashboard();
            }
            else
            {
                NotifyView("Error", "Unable to verify phone", "Unable to verify phone. Please check your Activation Code if it is valid. Or try resending a new code to your phone");
                return View();
            }
        }



        [HttpGet]
        [RequireSession]
        public ActionResult VerificationPending()
        {
            return View();
        }

        [HttpGet]
        [RequireSession]
        public ActionResult DocumentsRequired()
        {
            return View();
        }


        [HttpPost]
        [RequireSession]
        [ValidateAntiForgeryToken]
        public ActionResult DocumentsRequired(DocumentsRequired_VM vm)
        {
            SessionUser user = Session["User"] as SessionUser;
            UserVerification Verification = VerificationRepo.Get(user.ID);
            if (vm.IdentityDocument != null)
            {
                string fn = Path.GetFileName(vm.IdentityDocument.FileName);
                string fileExtension = fn.Remove(0, fn.IndexOf('.') + 1);
                fn =   $"IdentityDocument_{user.ID}." + fileExtension;
                string SaveLocation = "~/Images/VerificationImages/";
                vm.IdentityDocumentPath = Path.Combine(SaveLocation, fn);
                string FilePath = Server.MapPath(SaveLocation);
                vm.IdentityDocument.SaveAs(Path.Combine(FilePath, fn));

            }

            if (vm.BankStatement != null)
            {
                string fn = Path.GetFileName(vm.BankStatement.FileName);
                string fileExtension = fn.Remove(0, fn.IndexOf('.') + 1);
                fn = $"BankStatement_{user.ID}." + fileExtension;
                string SaveLocation = "~/Images/VerificationImages/";
                vm.BankStatementPath = Path.Combine(SaveLocation, fn);
                string FilePath = Server.MapPath(SaveLocation);
                vm.BankStatement.SaveAs(Path.Combine(FilePath, fn));

            }

            if (vm.ProofOfResidency != null)
            {
                string fn = Path.GetFileName(vm.ProofOfResidency.FileName);
                string fileExtension = fn.Remove(0, fn.IndexOf('.') + 1);
                fn = "ProofOfResidency." + fileExtension;
                string SaveLocation = "~/Images/VerificationImages/";
                vm.ProofOfResidencyPath = Path.Combine(SaveLocation, fn);
                string FilePath = Server.MapPath(SaveLocation);
                vm.ProofOfResidency.SaveAs(Path.Combine(FilePath, fn));

            }
            var UserDetails = vm.GetUserVerificationDetails();
            Verification.BankDocument = UserDetails.BankDocument;
            Verification.IdentityDocument = UserDetails.IdentityDocument;
            Verification.ProofOfResidency = UserDetails.ProofOfResidency;

            VerificationRepo.Update(Verification);
            return RedirectToAction("VerificationPending", "Account");
        }


        public ActionResult RedirectToDashboard()
        {
            SessionUser user = Session["User"] as SessionUser;
            if (user.Role.ID == (int)DefaultRole.Borrower)
            {
                return RedirectToAction("BorrowerDashboard", "Overview");
            }
            else if (user.Role.ID == (int)DefaultRole.Lender)
            {
                return RedirectToAction("LenderDashboard", "Overview");
            }
            else
            {
                return RedirectToAction("AdminDashboard", "Overview");
            }
        }

    }
}