﻿using CrowdFunding.Models.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CrowdFunding.Controllers
{
    public class CompanyController : PanelController
    {

        public CompanyRepository CompanyRepo { get; set; }

        public ActionResult Companies()
        {
            return View();
        }

        public ActionResult AddCompany()
        {
            return View();
        }
        public ActionResult DeleteCompany()
        {
            return View();
        }
    }
}