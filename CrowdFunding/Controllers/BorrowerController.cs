﻿using CrowdFunding.Models.Filters;
using CrowdFunding.Models.Repositories;
using CrowdFunding.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CrowdFunding.Controllers
{
    public class BorrowerController : PanelController
    {
        public UserRepository UserRepo { get; set; }
        public BorrowerController()
        {
            this.UserRepo = new UserRepository();
        }
        [Role("Borrower")]
        public ActionResult Dashboard()
        {
            return View(GetDashboardVM());
        }

        public Dashboard_VM GetDashboardVM()
        {
            Dashboard_VM vm = new Dashboard_VM();
            vm.LenderList = UserRepo.GetLenderList();
            vm.BorrowerList = UserRepo.GetBorrowerList();
            vm.Lenders = vm.LenderList.Count;
            vm.Borrowers = vm.BorrowerList.Count;
            return vm;
        }

    }
}