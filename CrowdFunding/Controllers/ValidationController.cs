﻿using CrowdFunding.Models.Repositories;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CrowdFunding.Controllers
{
    public class ValidationController : BaseController
    {
        public UserRepository UserRepo {get;set;}
        public ValidationController()
        {
            this.UserRepo = new UserRepository();
        }

        public JsonResult IsAvailableEmail(string email, int Id = 0)
        {
            if (!UserRepo.IsEmailRegistered(email, Id))
                return Json(true, JsonRequestBehavior.AllowGet);
            var suggestedUid = string.Format(CultureInfo.InvariantCulture, "{0} is already registered. Try {1}",
                email, "<a href='" + Url.Action("Login", "Account") + "'>Login</a><br />");
            return Json(suggestedUid, JsonRequestBehavior.AllowGet);
        }

        public JsonResult IsNotAvailableEmail(string email)
        {
            if (UserRepo.IsEmailRegistered(email, 0))
                return Json(true, JsonRequestBehavior.AllowGet);
            var suggestedUid = string.Format(CultureInfo.InvariantCulture, "{0} is not registered. Try {1}", email, "<a href='" + Url.Action("Register", "Home") + ">Register</a><br />");
            return Json(suggestedUid, JsonRequestBehavior.AllowGet);
        }
    }
}