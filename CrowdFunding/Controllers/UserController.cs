﻿using CrowdFunding.Models.DBModels;
using CrowdFunding.Models.Filters;
using CrowdFunding.Models.Repositories;
using CrowdFunding.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CrowdFunding.Controllers
{   
    public class UserController : PanelController
    {
        public UserRepository UserRepo { get; set; }
        public RoleRepository RoleRepo { get; set; }
        public VerificationRepository VerificationRepo { get; set; }
        public UserController()
        {
            this.UserRepo = new UserRepository();
            this.RoleRepo = new RoleRepository();
            this.VerificationRepo = new VerificationRepository();
        }

        [Role("Admin")]
        [HttpGet]
        public ActionResult UserManagement()
        {
            return View(UserRepo.GetList());
        }

        [HttpGet]
        public ActionResult AddUser(int? ID)
        {

            ViewBag.Roles = RoleRepo.GetList();
            if (user.Role.ID == (int)DefaultRole.Borrower || user.Role.ID == (int)DefaultRole.Lender)
            {
                User u = UserRepo.Get(user.ID);
                User_VM vm = new User_VM(u);
                return View(vm);
            }
            else if (ID == null)
            {
                return View(new User_VM());
            }
            else
            {
                User user = UserRepo.Get((int)ID);
                User_VM u = new User_VM(user);
                return View(u);
            }
        }

        [HttpPost]
        public ActionResult AddUser(User_VM user)
        {
            if (user.ImageFile != null)
            {
                string fn = Path.GetFileName(user.ImageFile.FileName);
                string fileExtension = fn.Remove(0, fn.IndexOf('.') + 1);
                fn = $"ProfileImage_{user.ID}{(new Random().NextDouble()) * 1000}." + fileExtension;
                string SaveLocation = "~/Images/Users/";

                user.ImagePath = Path.Combine(SaveLocation, fn);
                string FilePath = Server.MapPath(SaveLocation);
                user.ImageFile.SaveAs(Path.Combine(FilePath, fn));
            }


            if (user.ID == 0)
            {
                UserRepo.Insert(user);
            }
            else
            {
                UserRepo.Update(user);
            }
            return RedirectToAction("AddUser", "User");
        }

        public JsonResult DeleteUser(int UserID)
        {
            return Json(UserRepo.Delete(UserID));
        }


        [HttpGet]
        public ActionResult VerifyUser()
        {
            List<VerifyUser_LV> UserList = VerificationRepo.GetList();
            return View(UserList);
        }


        [HttpPost]
        public ActionResult VerifyUser(int UserID)
        {
            VerificationRepo.VerifyUser(UserID);
            return RedirectToAction("VerifyUser", "User");
        }

        [HttpPost]
        public ActionResult RemoveVerification(int UserID)
        {
            VerificationRepo.UnVerifyUser(UserID);
            return RedirectToAction("VerifyUser", "User");
        }

    }
}