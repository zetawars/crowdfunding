﻿using CrowdFunding.Controllers;
using CrowdFunding.Models.Other;
using CrowdFunding.Models.Repositories;
using System;
using System.Web.Mvc;

namespace CrowdFunding.Models.DBModels
{
    public class CredentialsVerifiedAttribute : ActionFilterAttribute
    {
        public UserRepository UserRepo{get;set;}
        public VerificationRepository VerificationRepo { get; set; }
        public CredentialsVerifiedAttribute()
        {
            this.UserRepo = new UserRepository();
            this.VerificationRepo = new VerificationRepository();
        }


        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var controller = (PanelController)filterContext.Controller;
            SessionUser user = controller.Session["User"] as SessionUser;
            if (user != null)
            {
                if (user.Role.ID != (int)DefaultRole.Admin)
                {
                    UserVerification Verification = VerificationRepo.Get(user.ID);
                    if (!Verification.PhoneVerified)
                    {
                        filterContext.Result = new RedirectToRouteResult(new System.Web.Routing.RouteValueDictionary { { "action", "PhoneVerification" }, { "controller", "Account" } });
                    }
                    else if (!Verification.EmailVerified || !Verification.PhoneVerified)
                    {
                        filterContext.Result = new RedirectToRouteResult(new System.Web.Routing.RouteValueDictionary { { "action", "EmailVerification" }, { "controller", "Account" } });
                    }
                    else if (!Verification.BankDocumentVerified || !Verification.IdentityDocumentVerified || !Verification.ProofOfResidencyVerified)
                    {
                        if (!string.IsNullOrWhiteSpace(Verification.IdentityDocument) || !string.IsNullOrWhiteSpace(Verification.ProofOfResidency) || !string.IsNullOrWhiteSpace(Verification.BankDocument))
                        {
                            filterContext.Result = new RedirectToRouteResult(new System.Web.Routing.RouteValueDictionary { { "action", "VerificationPending" }, { "controller", "Account" } });
                        }
                        else
                        {
                            filterContext.Result = new RedirectToRouteResult(new System.Web.Routing.RouteValueDictionary { { "action", "DocumentsRequired" }, { "controller", "Account" } });
                        }
                    }
                }
                
            }
        }

    }
}