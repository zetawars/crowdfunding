﻿
using CrowdFunding.Controllers;
using CrowdFunding.Models.DBModels;
using CrowdFunding.Models.Other;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CrowdFunding.Models.Filters
{
    public class RoleAttribute : ActionFilterAttribute
    {
        public List<string> roles { get; set; }
        public RoleAttribute(string role)
        {
            this.roles = new List<string>
            {
                role
            };

        }



        public RoleAttribute(string[] roles)
        {
            this.roles = roles.ToList();
        }
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var controller = (PanelController)filterContext.Controller;
            SessionUser user = controller.Session["User"] as SessionUser;
            if (user != null)
            {
                if (!this.roles.Contains(user.Role.Name))
                {
                    controller.Session.Clear();
                    if (controller.Request.Cookies["KMGQQMK"] != null)
                    {
                        var c = controller.Request.Cookies["KMGQQMK"];
                        c.Expires = DateTime.Now.AddDays(-1);
                        controller.Response.Cookies.Add(c);
                    }
                    filterContext.Result = new RedirectToRouteResult(new System.Web.Routing.RouteValueDictionary { { "action", "Index" }, { "controller", "Home" } });
                }
            }
        }
    }
}