﻿using CrowdFunding.Models.DBModels;

using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace CrowdFunding.Models.Repositories
{
    public class RoleRepository : DBRepository
    {
        public bool CreateDefaultRoles()
        {
            foreach (var el in Enum.GetValues(typeof(DefaultRole)).Cast<DefaultRole>())
            {
                Role role = ORM.Get<Role>("SELECT TOP 1 * FROM [Users].[Role] Where Name Like @Name","",new { ID = ((int)el).ToString(), Name = el.ToString() });
                if (role == null)
                {
                    ORM.Insert(new Role { ID = (int)el, Name = el.ToString() });
                }

            }
            return true;
        }

        public List<Role> GetList()
        {
            return ORM.GetList<Role>();
        }
    }
}