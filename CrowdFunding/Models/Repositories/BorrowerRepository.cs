﻿using CrowdFunding.Models.DBModels;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace CrowdFunding.Models.Repositories
{
    public class BorrowerRepository : DBRepository
    {
        public string TableName { get; set; }
        public BorrowerRepository()
        {
            this.TableName = "Borrowers";
        }

        public bool Insert(Borrower Borrower)
        {
            return ORM.Insert(Borrower);       
        }
    }
}