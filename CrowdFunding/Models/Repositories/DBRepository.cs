﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace CrowdFunding.Models.Repositories
{
    public class DBRepository
    {
        protected MiniORM ORM { get; set; }
        public DBRepository()
        {
            this.ORM = new MiniORM();
        }
    }
}