﻿using CrowdFunding.Models.DBModels;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace CrowdFunding.Models.Repositories
{
    public class LenderRepository : DBRepository
    {
        public LenderRepository()
        {
        }

        public bool Insert(Lender lender)
        {
            return ORM.Insert(lender);
        }
    }
}