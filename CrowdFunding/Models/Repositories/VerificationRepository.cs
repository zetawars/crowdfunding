﻿using CrowdFunding.Models.DBModels;
using CrowdFunding.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CrowdFunding.Models.Repositories
{
    public class VerificationRepository : DBRepository
    {
        public VerificationRepository()
        {

        }

        public UserVerification Get(int UserID)
        {
            return ORM.Get<UserVerification>("", "Where UserID = @UserID", new { UserID });
        }

        public bool Update(UserVerification verification)
        {
            return ORM.Update(verification,  "Where ID = @ID ", new { verification.ID });
        }

        public List<VerifyUser_LV> GetList(object Params = null)
        {
            string query = $@"
SELECT * FROM Users.[User] U INNER JOIN Users.[UserVerification] V 
ON U.ID = V.UserID Where EmailVerified = 1 And PhoneVerified = 1  AND U.IsDeleted = 0
And 
(IdentityDocument IS NOT NULL  AND IdentityDocumentVerified = 0)
OR
(BankDocument IS NOT NULL  AND BankDocumentVerified = 0)
OR 
(ProofOfResidency IS NOT NULL  AND ProofOfResidencyVerified = 0)
";
            return ORM.GetList<VerifyUser_LV>(query, "", Params);
        }

        public bool VerifyUser(int UserID)
        {
            string query = $@"
UPDATE Users.[UserVerification] SET IdentityDocumentVerified = 1, BankDocumentVerified = 1, ProofOfResidencyVerified = 1 where UserID = @UserID
";
            return ORM.ExecuteQuery(query, new { UserID});
        }
        public bool UnVerifyUser(int UserID)
        {
            string query = $@"
UPDATE Users.[UserVerification] SET IdentityDocumentVerified = 0, BankDocumentVerified = 0, ProofOfResidencyVerified = 0 where UserID = @UserID
";
            return ORM.ExecuteQuery(query, new { UserID });
        }
    }
}