﻿using CrowdFunding.Models.DBModels;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using CrowdFunding.Models.ViewModels;
using AutoMapper;
using CrowdFunding.Models.Other;

namespace CrowdFunding.Models.Repositories
{
    public class UserRepository : DBRepository
    {
        public bool Insert(User user)
        {
            return ORM.Insert(user); 
        }

        public (int, string, string) RegisterLender(Lender_VM vm)
        {
            string EmailCode = Common.RandomDigits(4);
            string PhoneCode = Common.RandomDigits(4);

            return (ORM.GetScaler("exec RegisterLender @FirstName, @LastName, @Email, @Password, @MobileNo,  @CountryID, @Sponsor, @EmailCode, @PhoneCode", 
                new { vm.FirstName, vm.LastName, vm.Email, vm.Password, vm.MobileNo, vm.CountryID, vm.Sponsor, EmailCode, PhoneCode }
                ), EmailCode, PhoneCode);
        }

        public (int, string, string) RegisterBorrower(Borrower_VM vm)
        {
            string EmailCode = Common.RandomDigits(4);
            string PhoneCode = Common.RandomDigits(4);

            //string query = @"exec RegisterBorrower @FirstName, @LastName, @Email, @Password, @MobileNo, @CompanyID, @AmountToBeFunded, @DurationOfFunding, @PriorityOfLoan,  @EmailCode, @PhoneCode";
            //return (ORM.GetScaler(query, new { vm.FirstName, vm.LastName, vm.Email, vm.Password, vm.MobileNo, vm.CompanyID, vm.AmountTobeFunded, vm.DurationOfFunding, vm.PriorityOfLoan, EmailCode, PhoneCode } 
            //), EmailCode, PhoneCode);

            string query = $@"exec RegisterBorrower '{vm.FirstName}', '{vm.LastName}', '{vm.Email}', '{vm.Password}','{vm.MobileNo}', '{vm.CompanyID}', '{vm.AmountTobeFunded}', '{vm.DurationOfFunding}', '{vm.Turnover}', '{vm.PriorityOfLoan}',  '{EmailCode}', '{PhoneCode}'";
            return (ORM.GetScaler(query, null
            ), EmailCode, PhoneCode);
        }

        public double GetWalletAmount(int ID)
        {
            string query = @"SELECT Amount FROM Users .UserAccount where UserID = @ID";
            return double.Parse(ORM.QueryColumn(query, new { ID }).First());
        }

        public bool IsEmailVerified(int iD)
        {
            string query = "SELECT EmailVerified From Users.[UserVerification] where ID = @ID";
            return ORM.GetScaler(query, new { ID=iD}) > 0;
        }

        public bool IsEmailRegistered(string Email, int ID)
        {
            string query = "Select count(*) from [Users].[User] where Email = @Email";
            if (ID != 0)
            {
                query += " AND ID != @ID";
            }
            return ORM.GetScaler(query, new { Email, ID}) > 0;
        }

        public List<User> GetBorrowerList()
        {
            string Query = "SELECT U.* FROM [Users].[User] U INNER JOIN [Users].[Borrower] B on U.ID = B.ID";
            return ORM.GetList<User>(Query);
        }

        public List<User> GetLenderList()
        {
            string Query = "SELECT U.* FROM Users.[User] U INNER JOIN Users.Lender L on U.ID = L.ID";
            return ORM.GetList<User>(Query);
        }

        public List<User> GetList()
        {
            return ORM.GetList<User>("","Where IsDeleted = 0");
        }

        public Role GetRole(int roleID)
        {
            string query = $"SELECT * FROM [Users].[Role] where ID = {roleID}";
            return ORM.Get<Role>(query);
        }

        public bool Update(User user)
        {
            return ORM.Update(user, "Where ID = @ID",new { user.ID});
        }

        public bool Delete(int userID)
        {
            string query = "UPDATE Users.[User] SET IsDeleted = 1 where ID = @ID";
            return ORM.ExecuteQuery(query, new { ID = userID });
        }

        public User Login(string Email, string Password)
        {
            string query = "SELECT * FROM Users.[User] where Email = @Email AND IsDeleted = 0 AND Password = @Password collate Latin1_General_CS_AS";
            return ORM.Get<User>(query,"", new { Email, Password});
        }


        public User Get(int userId)
        {
            return ORM.Get<User>("",$"Where ID = { userId}");
        }

        public bool ChangePassoword(int ID, string NewPassword)
        {
            string query = "UPDATE [Users].[User] SET Password = @NewPassword where ID = @ID";
            return ORM.ExecuteQuery(query, new { ID, NewPassword});
        }

        public bool VerifyEmail(int UserID, string Code)
        {
            string query = "SELECT COUNT(*) FROM Users.[ActivationCode] Where UserID = @UserID AND Code = @Code  AND  Type = 'Email'";
            return ORM.GetScaler(query, new { UserID, Code }) > 0;
        }

        public bool ConfirmEmail(int UserID)
        {
            string query = "UPDATE Users.[UserVerification] SET EmailVerified = 1 where UserID = @UserID";
            return ORM.ExecuteQuery(query, new { UserID });
        }


        public bool VerifyPhone(int UserID, string Code)
        {
            string query = "SELECT COUNT(*) FROM Users.[ActivationCode] Where UserID = @UserID AND Code = @Code AND Type = 'Phone'";
            return ORM.GetScaler(query, new { UserID, Code }) > 0;
        }

        public bool ConfirmPhone(int UserID)
        {
            string query = "UPDATE Users.[UserVerification] SET PhoneVerified = 1 where UserID = @UserID";
            return ORM.ExecuteQuery(query, new { UserID });
        }
    }
}