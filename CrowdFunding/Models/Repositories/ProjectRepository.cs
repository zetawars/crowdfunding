﻿using CrowdFunding.Models.DBModels;
using CrowdFunding.Models.ViewModels;
using CrowdFunding.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CrowdFunding.Models.Repositories
{
    public class ProjectRepository : DBRepository
    {
        public List<Project> GetList()
        {
            return ORM.GetList<Project>("","Where IsDeleted = 0");
        }
        public List<Project> GetList(int ID)
        {
            return ORM.GetList<Project>("", "Where IsDeleted = 0 AND UserID = @ID", new { ID });
        }
        public List<Project> GetListByUser()
        {
            string query = "";
            return ORM.GetList<Project>("", "Where IsDeleted = 0");
        }

        public Project Get(int ID)
        {
            return ORM.Get<Project>("", "Where ID = @ID", new { ID});
        }

        public List<Project_LV> GetPopularProjects()
        {
            string query = $@"

SELECT top 10 P.ID as ProjectID, P.Title, M.ImagePath, 
STUFF((
SELECT ', ' + CAST(T.Name AS VARCHAR(10)) [text()] FROM Projects.Tags  T LEFT JOIN Projects.ProjectTags PT ON T.ID = PT.TagID WHERE PT.ProjectID = P.ID FOR XML PATH(''), TYPE)
.value('.','NVARCHAR(MAX)'),1,2,' ') as Tags, 
P.Amount as Pledged, 
ISNULL(SUM(PT.TransferedAmount),0) as Funded, 
P.TimeLeft, 
U.ID as BorrowerID,
U.FirstName + ' ' + U.LastName as BorrowerName,
U.ImagePath as BorrowerImagePath
 FROM Projects.Project P 
OUTER APPLY ( SELECT TOP 1 ImagePath FROM Projects.ProjectImages Where ProjectID = P.ID order by ID) M
LEFT JOIN Projects.Project_Transactions PT ON PT.ProjectID = P.ID
LEFT JOIN Users.[Borrower] B ON B.ID = P.UserID
LEFT JOIN Users.[User] U ON U.ID = B.ID
Where P.IsDeleted = 0
And U.IsDeleted = 0 
GROUP BY P.ID, P.Title, P.Amount, M.ImagePath, P.TimeLeft, P.Popularity,  U.ID, U.FirstName, U.LastName, U.ImagePath
Order by P.Popularity desc

";
            return ORM.GetList<Project_LV>(query);
        }

        public List<Project_LV> GetFeaturedProjects()
        {
            string query = $@"
SELECT P.ID as ProjectID, P.Title, M.ImagePath, 
STUFF((
SELECT ', ' + CAST(T.Name AS VARCHAR(10)) [text()] FROM Projects.Tags  T LEFT JOIN Projects.ProjectTags PT ON T.ID = PT.TagID WHERE PT.ProjectID = P.ID FOR XML PATH(''), TYPE)
.value('.','NVARCHAR(MAX)'),1,2,' ') as Tags, 
P.Amount as Pledged, 
ISNULL(SUM(PT.TransferedAmount),0) as Funded, 
P.TimeLeft, 
U.ID as BorrowerID,
U.FirstName + ' ' + U.LastName as BorrowerName,
U.ImagePath as BorrowerImagePath
 FROM Projects.Project P 
OUTER APPLY ( SELECT TOP 1 ImagePath FROM Projects.ProjectImages Where ProjectID = P.ID order by ID) M
LEFT JOIN Projects.Project_Transactions PT ON PT.ProjectID = P.ID
LEFT JOIN Users.[Borrower] B ON B.ID = P.UserID
LEFT JOIN Users.[User] U ON U.ID = B.ID
Where P.IsDeleted = 0 And P.Featured = 1
And U.IsDeleted = 0
GROUP BY P.ID, P.Title, P.Amount, M.ImagePath, P.TimeLeft, U.ID, U.FirstName, U.LastName, U.ImagePath
";
            return ORM.GetList<Project_LV>(query);
        }

        public bool Insert(Project project)
        {
            return ORM.InsertAndGetID(project);
        }

        public bool Update(Project project)
        {
            return ORM.Update(project, "Where ID = @ID", new { project.ID});
        }

        public List<string> GetProjectImages(int ID)
        {
            return ORM.QueryColumn("SELECT ImagePath FROM Projects.ProjectImages Where ProjectID = @ID", new { ID});
        }

        public double GetPlegedAmount(int ID)
        {
            string query = "SELECT ISNULL(SUM(TransferedAmount),0) as Amount FROM Projects.Project_Transactions Where ProjectID = @ID";
            return double.Parse(ORM.QueryColumn(query, new { ID}).First());
        }

        public List<Tag> GetAllTags()
        {
            string query = @"SELECT * FROM Projects.Tags";
            return ORM.GetList<Tag>(query, "");
        }

        public List<Tag> GetProjectTags(int ID) 
        {
            string query = @"
SELECT T.* FROM Projects.ProjectTags PT
INNER JOIN Projects.Tags T ON PT.TagID = T.ID
Where PT.ProjectID = @ID
";
            return ORM.GetList<Tag>(query, "", new { ID });
        }

        public List<User> GetInvestors(int ID)
        {
            string query = @"SELECT DISTINCT U.* From Projects.Project_Transactions PT INNER JOIN[Users].[User] U ON U.ID = PT.LenderID";
            return ORM.GetList<User>(query);
        }

        public List<string> GetImages(int ID)
        {
            string query = "SELECT ImagePath FROM Projects.ProjectImages Where ProjectID = @ID";
            return ORM.QueryColumn(query, new { ID });
        }
     
        public bool InsertImages(int ID, List<string> projectImages)
        {
            string query = string.Empty;
            foreach (var element in projectImages)
            {
                query += $"INSERT INTO Projects.ProjectImages (ProjectID, ImagePath) VALUES (@ID, '{element}');";
            }
            return ORM.ExecuteQuery(query, new { ID });
        }

        public bool UpdateImages(int ID, List<string> projectImages)
        {
            string query = "DELETE FROM Projects.ProjectImages Where ProjectID = @ID;";
            foreach (var element in projectImages)
            {
                query += $"INSERT INTO Projects.ProjectImages (ProjectID, ImagePath) VALUES (@ID, '{element}');";
            }
            return ORM.ExecuteQuery(query, new { ID });
        }


        public bool InsertTags(int ID, List<string> selectedTags)
        {
            string query = string.Empty;
            foreach (var element in selectedTags)
            {
                query += $"INSERT INTO Projects.ProjectTags (ProjectID, TagID) VALUES (@ID, '{element}');";
            }
            return ORM.ExecuteQuery(query, new { ID });
        }

        public bool DeleteProject(int ID)
        {
            string query = "UPDATE Projects.Project SET IsDeleted = 1 where ID = @ID";
            return ORM.ExecuteQuery(query, new { ID });
        }

        public bool UpdateTags(int ID, List<string> selectedTags)
        {
            string query = "DELETE FROM Projects.ProjectTags Where ProjectID = @ID;";
            foreach (var element in selectedTags)
            {
                query += $"INSERT INTO Projects.ProjectTags (ProjectID, TagID) VALUES (@ID, '{element}');";
            }
            return ORM.ExecuteQuery(query, new { ID });
        }

        public bool CancelProject(int ProjectID, int UserID)
        {
            return ORM.ExecuteQuery("UPDATE Projects.Project SET Approved = 0, ApprovedBy = NULL where ID = @ProjectID ", new { ProjectID, UserID });
        }

        public bool ConfirmProject(int ProjectID, int UserID)
        {
            return ORM.ExecuteQuery("UPDATE Projects.Project SET Approved = 1, ApprovedBy = @UserID where ID = @ProjectID ", new { ProjectID, UserID });
        }



        public List<ProjectFunding> GetProjectFundings(int ID, int Role)
        {
            string query = $@"

SELECT 
P.ID as ProjectID, P.Title as ProjectTitle , P.Goal,
B.ID as BorrowerID , B.FirstName +' '+  B.LastName As BorrowerName,
PT.TransferedAmount as Amount, PT.TimeStamp as TransactionDate,
L.ID as LenderID , L.FirstName +' '+  L.LastName As LenderName
FROM Projects.Project P 
INNER JOIN Users.[User] as B ON B.ID = P.UserID
INNER JOIN Projects.Project_Transactions PT ON PT.ProjectID = P.ID
INNER JOIN Users.[User] as L ON L.ID = PT.LenderID

";
            if (Role == (int)DefaultRole.Borrower)
            {
                query += " Where L.ID = @ID";
                return ORM.GetList<ProjectFunding>(query, "", new { ID});
            }
            else if (Role == (int)DefaultRole.Lender)
            {
                query += " Where B.ID = @ID";
                return ORM.GetList<ProjectFunding>(query, "", new { ID});
            }
            else
            {
                return ORM.GetList<ProjectFunding>(query);
            }
        }
    }
}