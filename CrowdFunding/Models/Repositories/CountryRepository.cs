﻿using CrowdFunding.Models.DBModels;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace CrowdFunding.Models.Repositories
{
    public class CountryRepository : DBRepository
    {
        public CountryRepository()
        {
        }

        public List<Country> GetList()
        {
            return ORM.GetList<Country>();
        }
    }
}