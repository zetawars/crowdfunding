﻿using CrowdFunding.Models.DBModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CrowdFunding.Models.ViewModels
{
    public class VerifyUser_LV : User
    {
        public int UserID { get; set; }
        public bool EmailVerified { get; set; }
        public bool PhoneVerified { get; set; }
        public string IdentityDocument { get; set; }
        public string ProofOfResidency { get; set; }
        public string BankDocument { get; set; }
        public bool IdentityDocumentVerified { get; set; }
        public bool ProofOfResidencyVerified { get; set; }
        public bool BankDocumentVerified { get; set; }
    }
}