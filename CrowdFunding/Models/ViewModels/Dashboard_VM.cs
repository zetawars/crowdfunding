﻿using CrowdFunding.Models.DBModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CrowdFunding.Models.ViewModels
{
    public class Dashboard_VM
    {
        public int Lenders { get; set; }
        public int Borrowers { get; set; }
        public List<User> LenderList { get; set; }
        public List<User> BorrowerList { get; set; }
        public Dashboard_VM()
        {
            this.LenderList = new List<User>();
            this.BorrowerList = new List<User>();
            this.Lenders = 0;
            this.Borrowers = 0;
        }

    }
}