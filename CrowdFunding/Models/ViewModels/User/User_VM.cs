﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CrowdFunding.Models.DBModels;

namespace CrowdFunding.Models.ViewModels
{
    public class User_VM : CrowdFunding.Models.DBModels.User
    {

      
        public HttpPostedFileBase ImageFile { get; set; }

        public User_VM() {
        }

        public User_VM(User user)
        {
            this.ID = user.ID;
            this.FirstName = user.FirstName;
            this.LastName = user.LastName;
            this.Email = user.Email;
            this.Password = user.Password;
            this.RoleID = user.RoleID;
            this.IsDeleted = user.IsDeleted;
            this.MobileNo = user.MobileNo;
            this.TimeStamp = user.TimeStamp;
            this.Address = user.Address;
            this.ImagePath = user.ImagePath;
        }



    }
}