﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CrowdFunding.Models.ViewModels
{
    public class ProjectFunding
    {
        public int ProjectID { get; set; }
        public int LenderID { get; set; }
        public int BorrowerID { get; set; }
        public string ProjectTitle { get; set; }
        public string LenderName { get; set; }
        public string BorrowerName { get; set; }
        public string Amount { get; set; }
        public string Goal { get; set; }
        public DateTime TransactionDate { get; set; }
    }

}