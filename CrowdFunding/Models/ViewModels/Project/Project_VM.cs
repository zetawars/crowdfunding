﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CrowdFunding.Models.DBModels;

namespace CrowdFunding.Models.ViewModels
{
    public class Project_VM
    {
        public Project project { get; set; }
        public List<HttpPostedFileBase> ImageFiles { get; set; }
        public double Pledged { get; set; }
        public int Backers { get; set; } 
        public List<User> Investors { get; set; }
        public List<string> SelectedTags { get; set; }
        public List<string> ProjectImages { get; set; }
        public List<Tag> Tags { get; set; }
        
        public Borrower ProjectOwner { get; set; }
        public Project_VM()
        {
            this.project = new Project();
            this.ImageFiles = new List<HttpPostedFileBase>();
            this.Pledged = 0;
            this.Backers = 0;
            this.Investors = new List<User>();
            this.SelectedTags = new List<string>();
            this.ProjectImages = new List<string>();
            this.Tags = new List<Tag>();
        }
    }
}