﻿using CrowdFunding.Models.DBModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CrowdFunding.Models.ViewModels
{
    public class ChangePassword_VM
    {
        [Required]
        public string OldPassword { get; set; }
        [Required]
        public string NewPassword { get; set; }
        [Required]
        [Compare("NewPassword")]
        public string ConfirmPassword { get; set; }

    }

    public class Register_VM
    {
        public Lender_VM lender { get; set; }
        public Borrower_VM borrower { get; set; }
        public List<Country> Countries { get; set; }
        public Register_VM()
        {
            this.lender = new Lender_VM();
            this.borrower = new Borrower_VM();
        }
    }

    public class Lender_VM
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Sponsor { get; set; }
        public int CountryID { get; set; }
        public string MobileNo { get; set; }
        public Lender_VM()
        {
            this.FirstName = string.Empty;
            this.LastName = string.Empty;
            this.Email = string.Empty;
            this.MobileNo = string.Empty;
            this.Password = string.Empty;
            this.Sponsor = string.Empty;
            this.CountryID = 0;
        }
    }


    public class Borrower_VM
    {
        public int ID { get; set; }
        public string CompanyID { get; set; }
        public string AmountTobeFunded { get; set; }
        public string DurationOfFunding { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Turnover { get; set; }
        public string PriorityOfLoan { get; set; }
        public string MobileNo { get; set; }

        public Borrower_VM()
        {
            this.CompanyID = string.Empty ;
            this.AmountTobeFunded =string.Empty;
            this.DurationOfFunding = string.Empty;
            this.FirstName = string.Empty;
            this.LastName = string.Empty;
            this.Email = string.Empty;
            this.Password = string.Empty;
            this.Turnover = string.Empty;
            this.PriorityOfLoan = string.Empty;
        }
    }



    public class DocumentsRequired_VM
    {
        public string ProofOfResidencyPath { get; set; }
        public string IdentityDocumentPath { get; set; }
        public string BankStatementPath { get; set; }
        public HttpPostedFileBase ProofOfResidency { get; set; }
        public HttpPostedFileBase IdentityDocument { get; set; }
        public HttpPostedFileBase BankStatement { get; set; }

        public UserVerification GetUserVerificationDetails()
        {
            return new UserVerification
            {
                ProofOfResidency = ProofOfResidencyPath,
                IdentityDocument = this.IdentityDocumentPath,
                BankDocument = BankStatementPath
            };
        }

    }
}