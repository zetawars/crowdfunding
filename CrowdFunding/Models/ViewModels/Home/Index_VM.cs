﻿using System.Collections.Generic;

namespace CrowdFunding.Models.ViewModels
{
    public class Index_VM
    {
        public List<Project_LV> FeaturedProjects { get; set; }
        public List<Project_LV> PopularProjects { get; set; }
    }


    public class Project_LV
    {
        public int ProjectID { get; set; }
        public string Title { get; set; }
        public string ImagePath { get; set; }
        public string Tags { get; set; }
        
        public string Pledged { get; set; }
        public string Funded { get; set; }
        public string TimeLeft { get; set; }
        public string BorrowerID { get; set; }
        public string BorrowerName { get; set; }
        public string BorrowerImagePath { get; set; }
    }
}