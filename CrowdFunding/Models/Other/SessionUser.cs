﻿using CrowdFunding.Models.DBModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CrowdFunding.Models.Other
{
    public class SessionUser
    {
        public string Username { get; set; }
        public string Email { get; set; }
        public string MobileNo { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime TimeStamp { get; set; }
        public Role Role { get; set; }
        public int ID { get; set; }


        public SessionUser(User user, Role role)
        {
            this.ID = user.ID;
            this.Email = user.Email;
            this.FirstName = user.FirstName;
            this.LastName = user.LastName;
            this.MobileNo = user.MobileNo;
            this.TimeStamp = user.TimeStamp;
            this.Role = role;
        }
    }
}