﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;

namespace CrowdFunding.Models.Other
{
    public class SMSSender
    {

        public static bool SendSMS(string phone, string username, string password, string activation_code = "")
        {
            string Message = "This is your confirmation Code";
            if (!string.IsNullOrWhiteSpace(activation_code))
            {
                Message += " \nActivation Code : " + activation_code;
            }
            Message += "\n";
            return send_sms(phone, Message);
        }


        public static bool send_sms(string phone, string Message)
        {
            try
            {
                string temp = phone.Substring(0, 1);
                if (temp == "0" && phone.Count() == 11)
                {
                    phone = phone.Remove(0, 1);
                    phone = "92" + phone;
                }
                else if (temp == "+" && phone.Count() == 13)
                {
                    phone = phone.Remove(0, 1);
                }
                string request = "http://api.com" + Message + " &=" + phone + "&language=English";
                WebClient client = new WebClient();

                //if (!ServerConfig.server_path.Contains("localhost") || true)
                //{
                //    client.DownloadString(request);
                //}

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

    }
}