﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Net.Mime;
using System.Web;

namespace CrowdFunding.Models.Other
{
    public class EmailSender
    {
        public static bool SendEmail(string Email, string Subject, string Message)
        {
            try
            {
                string Body = Message;
                MailMessage mailMsg = new MailMessage();
                // To
                mailMsg.To.Add(new MailAddress(Email));
                // From
                mailMsg.From = new MailAddress("zetawars@hotmail.com", "Finvest User");
                mailMsg.Subject = Subject;
                string html = @"" + Body;
                mailMsg.IsBodyHtml = true;
                mailMsg.AlternateViews.Add(AlternateView.CreateAlternateViewFromString(html, null, MediaTypeNames.Text.Html));
                // Init SmtpClient and send
                SmtpClient smtpClient = new SmtpClient("smtp-mail.outlook.com", 587);
                smtpClient.EnableSsl = true;
                System.Net.NetworkCredential credentials = new System.Net.NetworkCredential("zetawars@hotmail.com", "KingOfPirates700");
                smtpClient.Credentials = credentials;
                smtpClient.Send(mailMsg);
                return true;
            }
            catch (Exception ex)
            {

                return false;
            }

        }


        public static void SignUpMail(string Path, string RecieverMail, string ActivationCode)
        {
            string Body = System.IO.File.ReadAllText(Path);
            Body = Body.Replace("#code_here#", ActivationCode);
            SendEmail(RecieverMail, "Email Confirmation", Body);
        }
    }
}

