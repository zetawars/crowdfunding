﻿using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Text;
using CrowdFunding.Models.DBModels;
//Author : Shazaki Zetawars //
namespace System
{
    public class DB_Common
    {
        protected static List<PropertyInfo> GetReadableProperties<T>()
        {
            return typeof(T).GetProperties().Where(x => !(Attribute.IsDefined(x, typeof(DontLoad)))).ToList();
        }
    }


    public class MiniORM : DB_Common
    {
        private string ConnectionString = Configuration.ConfigurationManager.ConnectionStrings["DefaultConnectionString"].ToString();

        
        #region InsertFunctions
        public bool Insert<T>(T _Object, string schemaName = null, string tableName = null)
        {
            return ExecuteQuery(QueryMaker.InsertQuery(_Object, schemaName, tableName));
        }
        public bool InsertAndGetID<T>(T _Object, string schemaName = null, string tableName = null)
        {
            string query = QueryMaker.InsertQuery(_Object, schemaName, tableName) + "SELECT SCOPE_IDENTITY();";
            int ID = GetScaler(query);
            foreach (var prop in _Object.GetType().GetProperties().Where(x=> (Attribute.IsDefined(x, typeof(Key)))).ToList())
            {
                prop.SetValue(_Object, ID);
            }
            return true;
        }

        #endregion

        #region UpdateFunctions
        public bool Update<T>(T _Object, string whereClause, object Params, string schemaName = null, string tableName = null)
        {
            return ExecuteQuery(QueryMaker.UpdateQuery(_Object, whereClause, schemaName, tableName), Params);
        }
        #endregion

        #region DeleteFunctions
        public bool Delete<T>(string whereClause, object Params, string schemaName = null, string tableName = null)
        {
            return ExecuteQuery(QueryMaker.DeleteQuery<T>(whereClause), Params);
        }
        #endregion

        #region OtherFunctions
        public int GetScaler(string query, object Params = null)
        {
            using (SqlConnection Connection = new SqlConnection(ConnectionString))
            {
                Connection.Open();
                SqlCommand cmd = GetSqlCommandWithParams(query, Connection, Params);
                int count = Convert.ToInt32(cmd.ExecuteScalar());
                Connection.Close();
                return count;
            }
        }
        public bool ExecuteQuery(string query, object Params = null)
        {
            using (SqlConnection Connection = new SqlConnection(ConnectionString))
            {
                Connection.Open();
                SqlCommand cmd = GetSqlCommandWithParams(query, Connection, Params);
                int rowseffected = cmd.ExecuteNonQuery();
                Connection.Close();
                return true;
            }
        }
        public bool ExecuteTransactionQuery(string query, object Params = null)
        {
            query = $"{QueryMaker.BeginTransQuery()} {query} {QueryMaker.CommitTransQuery()}";
            using (SqlConnection Connection = new SqlConnection(ConnectionString))
            {
                Connection.Open();
                SqlCommand cmd = GetSqlCommandWithParams(query, Connection, Params);
                int rowseffected = cmd.ExecuteNonQuery();
                Connection.Close();
                return true;
            }
        }
        #endregion

        #region ReadFunctions
        public List<Dictionary<string, string>> QueryList(string query, object Params = null)
        {
            List<Dictionary<string, string>> appList = new List<Dictionary<string, string>>();
            using (SqlConnection Connection = new SqlConnection(ConnectionString))
            {
                Connection.Open();
                SqlCommand cmd = GetSqlCommandWithParams(query, Connection, Params);
                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        Dictionary<string, string> app = new Dictionary<string, string>();
                        for (int i = 0; i < reader.FieldCount; i++)
                        {
                            app.Add(reader.GetName(i), reader[reader.GetName(i)].ToString());
                        }
                        appList.Add(app);
                    }
                }
                Connection.Close();
            }
            return appList;
        }
        public PageListObject<T> GetPagedList<T>(string query, string sortBy, string order, int numberOfRecords, int pageNumber, Dictionary<string, string> Params)
        {
            int offset = numberOfRecords * pageNumber;
            var Querries = QueryMaker.GetPagerQueries<T>(query, sortBy, order, offset, numberOfRecords);
            string mainquery = Querries.Query;
            string countquery = Querries.CountQuery;
            return PagedResults<T>(numberOfRecords, pageNumber, Params, offset, mainquery, countquery);
        }
        public PageListObject<T> GetPagedList<T>(string query, Dictionary<string, string> sortAndOrder, int numberOfRecords, int pageNumber, Dictionary<string, string> Params)
        {
            int offset = numberOfRecords * pageNumber;
            var Querries = QueryMaker.GetPagerQueries<T>(query, sortAndOrder, offset, numberOfRecords);
            string mainquery = Querries.Query;
            string countquery = Querries.CountQuery;
            return PagedResults<T>(numberOfRecords, pageNumber, Params, offset, mainquery, countquery);
        }
        private PageListObject<T> PagedResults<T>(int numberOfRecords, int pageNumber, object Params, int offset, string mainquery, string countquery)
        {

            int count = 0;
            List<T> results = new List<T>();
            using (SqlConnection Connection = new SqlConnection(ConnectionString))
            {
                Connection.Open();
                SqlCommand cmd = GetSqlCommandWithParams(mainquery, Connection, Params);
                SqlDataReader reader = cmd.ExecuteReader();
                var properties = GetReadableProperties<T>();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        var item = Activator.CreateInstance<T>();
                        foreach (var property in properties)
                        {
                            if (!reader.IsDBNull(reader.GetOrdinal(property.Name)))
                            {
                                DBReader(item, property, reader);
                            }
                        }
                        results.Add(item);
                    }
                }
                reader.Close();
                cmd = GetSqlCommandWithParams(countquery, Connection, Params);
                count = (int)cmd.ExecuteScalar();
                Connection.Close();
                Pager pager = GetPagerSettings(numberOfRecords, pageNumber, count, offset);
                return new PageListObject<T> { pager = pager, Results = results };
            }
        }
        private Pager GetPagerSettings(int numberOfRecords, int pageNumber, int count, int offset)
        {
            Pager pager = new Pager();
            pager.RecordsPerPage = numberOfRecords;
            pager.Offset = offset;
            pager.Fetch = numberOfRecords;
            pager.TotalRecords = count;
            pager.PageNumber = pageNumber;
            double d = pager.TotalRecords / pager.RecordsPerPage;
            pager.TotalPages = Convert.ToInt32(Math.Ceiling(d));
            return pager;
        }


        public Dictionary<string, string> QueryRow(string query, object Params = null)
        {
            Dictionary<string, string> appList = new Dictionary<string, string>();
            using (SqlConnection Connection = new SqlConnection(ConnectionString))
            {
                Connection.Open();
                SqlCommand cmd = GetSqlCommandWithParams(query, Connection, Params);
                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        for (int i = 0; i < reader.FieldCount; i++)
                        {
                            appList.Add(reader.GetName(i), reader[reader.GetName(i)].ToString());
                        }
                        break;
                    }
                }
                Connection.Close();
            }
            return appList;
        }
        public List<string> QueryColumn(string query, object Params)
        {
            List<string> list = new List<string>();
            using (SqlConnection Connection = new SqlConnection(ConnectionString))
            {
                Connection.Open();
                SqlCommand cmd = GetSqlCommandWithParams(query, Connection, Params);
                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        list.Add(reader[reader.GetName(0)].ToString());
                    }
                }
                Connection.Close();
            }
            return list;
        }
        public T Get<T>(string query = null, string whereClause = null, object Params = null)
        {
            T t = Activator.CreateInstance<T>();
            List<PropertyInfo> properties = GetReadableProperties<T>();
            query = QueryMaker.SelectQuery<T>(query, whereClause);
            using (SqlConnection Connection = new SqlConnection(ConnectionString))
            {
                Connection.Open();
                SqlCommand cmd = GetSqlCommandWithParams(query, Connection, Params);
                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        foreach (var property in properties)
                        {
                            DBReader(t, property, reader);
                        }
                    }
                }
                Connection.Close();
            }
            return t;
        }
        public bool Get<T>(T _Object, string query = "", string where_clause = "", object Params = null)
        {
            List<PropertyInfo> _properties = GetReadableProperties<T>();
            query = QueryMaker.SelectQuery<T>(query, where_clause);

            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                SqlCommand cmd = GetSqlCommandWithParams(query, connection, Params);
                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        foreach (PropertyInfo pi in _properties)
                        {
                            DBReader(_Object, pi, reader);
                        }
                    }
                }
                connection.Close();
                return true;
            }
        }
        public List<T> GetList<T>(string query = null, string whereClause = null, object Params = null)
        {
            var results = new List<T>();
            var properties = GetReadableProperties<T>();
            query = QueryMaker.SelectQuery<T>(query, whereClause);
            using (SqlConnection Connection = new SqlConnection(ConnectionString))
            {
                Connection.Open();
                SqlCommand cmd = GetSqlCommandWithParams(query, Connection, Params);
                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        var item = Activator.CreateInstance<T>();
                        foreach (var property in properties)
                        {
                            DBReader(item, property, reader);
                        }
                        results.Add(item);
                    }
                }
                Connection.Close();
                return results;
            }
        }
        public DataTable ReadDataTable(string query)
        {
            DataTable dt = new DataTable();
            using (SqlConnection Connection = new SqlConnection())
            {
                SqlDataAdapter sda = new SqlDataAdapter();
                Connection.Open();
                SqlCommand cmd = new SqlCommand(query, Connection);
                sda.SelectCommand = cmd;
                sda.Fill(dt);
                return dt;
            }

        }
        #endregion
        #region Private methods   
        private SqlCommand GetSqlCommandWithParams(string query, SqlConnection Connection, object Params)
        {
            SqlCommand cmd = new SqlCommand(query, Connection);
            if (Params != null)
            {
                foreach (var element in Params.GetType().GetProperties())
                {
                    cmd.Parameters.AddWithValue(element.Name, element.GetValue(Params));
                }
            }
            return cmd;
        }
        private void DBReader(Object _Object, PropertyInfo property, SqlDataReader reader)
        {
            if (!reader.IsDBNull(reader.GetOrdinal(property.Name)))
            {
                Type convertTo = Nullable.GetUnderlyingType(property.PropertyType) ?? property.PropertyType;
                property.SetValue(_Object, Convert.ChangeType(reader[property.Name], convertTo), null);
            }
            else
            {
                property.SetValue(_Object, null);
            }
        }
        #endregion Private methods
    }

    public class QueryMaker : DB_Common
    {
        #region Public methods
        public static string SelectQuery<T>(string query = null, string where_clause = null)
        {
            StringBuilder queryBuilder = new StringBuilder();
            if (string.IsNullOrWhiteSpace(query))
            {
                List<PropertyInfo> _properties = GetReadableProperties<T>();
                queryBuilder.Append("SELECT ");
                foreach (var element in _properties)
                {
                    queryBuilder.Append($"[{element.Name}], ");
                }
                queryBuilder.Length -= 2;
                queryBuilder.Append($"FROM {GetSchemaName<T>()}.{GetTableName<T>()} { where_clause }");
            }
            else
            {
                queryBuilder.Append(query);
                queryBuilder.Append($" { where_clause} ");
            }
            return queryBuilder.ToString();
        }
        public static string InsertQuery<T>(T _Object, string schemaName = null, string tableName = null)
        {
            List<PropertyInfo> properties = GetInsertProperties<T>();
            StringBuilder ColumnQueryBuilder = new StringBuilder();
            StringBuilder ValuesQueryBuilder = new StringBuilder();
            ColumnQueryBuilder.Append($"INSERT INTO {schemaName ?? GetSchemaName<T>()}.{tableName ?? GetTableName<T>()}");
            ColumnQueryBuilder.Append("(");
            ValuesQueryBuilder.Append(" VALUES (");
            foreach (var pi in properties)
            {
                ColumnQueryBuilder.Append($"{GetColumnName(pi)} , ");
                ValuesQueryBuilder.Append($"{ValueReader(pi.GetValue(_Object))}, ");
            }
            ColumnQueryBuilder.Length -= 2;
            ColumnQueryBuilder.Append(")");
            ValuesQueryBuilder.Length -= 2;
            ValuesQueryBuilder.Append(")");
            ColumnQueryBuilder.Append(ValuesQueryBuilder.ToString());
            return ColumnQueryBuilder.ToString();
        }
        public static string UpdateQuery<T>(T _Object, string whereClause, string schemaName = null, string tableName = null)
        {
            List<PropertyInfo> properties = GetUpdateProperties<T>();
            StringBuilder UpdateQueryBuilder = new StringBuilder();
            UpdateQueryBuilder.Append($"UPDATE {schemaName ?? GetSchemaName<T>()}.{tableName ?? GetTableName<T>()} SET ");
            foreach (var pi in properties)
            {
                UpdateQueryBuilder.Append($"{GetColumnName(pi)} = {ValueReader(pi.GetValue(_Object))}, ");
            }
            UpdateQueryBuilder.Length -= 2;
            UpdateQueryBuilder.Append($" {whereClause}");
            return UpdateQueryBuilder.ToString();
        }
        public static string DeleteQuery<T>(string whereClause, string schemaName = null, string tableName = null)
        {
            return $"Delete from {schemaName ?? GetSchemaName<T>()}.{tableName ?? GetTableName<T>()} {whereClause};";
        }
        public static string BeginTransQuery()
        {
            string query = string.Empty;
            query +=
                "BEGIN TRY " +
                "BEGIN TRANSACTION ";
            return query;
        }
        public static string CommitTransQuery()
        {
            string query = string.Empty;
            query +=
                "COMMIT " +
                "END TRY " +
                "BEGIN CATCH " +
                "declare @ErrorMessage nvarchar(max), @ErrorSeverity int, @ErrorState int;" +
                "select @ErrorMessage = ERROR_MESSAGE() + ' Line ' + cast(ERROR_LINE() as nvarchar(5)), @ErrorSeverity = ERROR_SEVERITY(), @ErrorState = ERROR_STATE();" +
                "rollback transaction;" +
                "raiserror(@ErrorMessage, @ErrorSeverity, @ErrorState);" +
                "END CATCH ";
            return query;
        }
        public static PagedQuery GetPagerQueries<T>(string query, string sortBy, string order, int offset, int numberOfRecords)
        {
            string mainquery = $"SELECT * FROM ({  query ?? SelectQuery<T>(query, "")}) MyList order by {sortBy} {order} offset {offset} rows fetch next {numberOfRecords} rows only";
            string countquery = $"SELECT COUNT(*) FROM ({  query ?? SelectQuery<T>(query, "") }) MyList";
            return new PagedQuery { Query = mainquery, CountQuery = countquery };
        }
        public static PagedQuery GetPagerQueries<T>(string query, Dictionary<string, string> sortAndOrder, int offset, int numberOfRecords)
        {
            string mainquery = $"SELECT * FROM ({ query ?? SelectQuery<T>(query, "")}) MyList {MultipleOrderByQuery(sortAndOrder, numberOfRecords, offset)}";
            string countquery = $"SELECT COUNT(*) FROM ({ query ?? SelectQuery<T>(query, "")}) MyList";
            return new PagedQuery { Query = mainquery, CountQuery = countquery };
        }
        #endregion Public methods

        #region Private methods

        private static List<PropertyInfo> GetInsertProperties<T>()
        {
            return typeof(T).GetProperties(System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.DeclaredOnly).ToList().Where(x => !(Attribute.IsDefined(x, typeof(DontInsert))) && !(Attribute.IsDefined(x, typeof(Key)))).ToList();
        }
        private static List<PropertyInfo> GetUpdateProperties<T>()
        {
            return typeof(T).GetProperties(System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.DeclaredOnly).ToList().Where(x => !(Attribute.IsDefined(x, typeof(DontUpdate))) && !(Attribute.IsDefined(x, typeof(Key)))).ToList();
        }

        private static string GetColumnName(PropertyInfo pi)
        {
            if (Attribute.IsDefined(pi, typeof(Column)))
            {
                Column k = (Column)Attribute.GetCustomAttribute(pi, typeof(Column));
                return $"[{k.Name}]";
            }
            else
            {
                return $"[{pi.Name}]";
            }
        }
        private static string GetTableName<T>()
        {
            if (Attribute.IsDefined(typeof(T), typeof(Table)))
            {
                Table t = (Table)Attribute.GetCustomAttribute(typeof(T), typeof(Table));
                return $"[{t.TableName }]";
            }
            else
            {
                return $"[{typeof(T).Name}]";
            }
        }
        private static string GetSchemaName<T>()
        {
            if (Attribute.IsDefined(typeof(T), typeof(Schema)))
            {
                Schema t = (Schema)Attribute.GetCustomAttribute(typeof(T), typeof(Schema));
                return $"[{t.SchemaName}]";
            }
            else
            {
                return "[dbo]";
            }
        }
        private static string ValueReader(Object value)
        {
            if (value == null)
            {
                return "NULL";
            }
            else if (value.GetType() == typeof(DateTime))
            {
                DateTime date = (DateTime)value;
                return $"'{date.ToString("yyyy-MM-dd hh:mm:ss")}'";
            }
            else
            {
                return $"'{value.ToString().Replace("'", "''")}'";
            }
        }
        private static string MultipleOrderByQuery(Dictionary<string, string> sortByAndOrder, int numberOfRecords, int offset)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(" order by ");
            foreach (var element in sortByAndOrder)
            {
                sb.Append(element.Key);
                sb.Append($" {element.Value}, ");
            }
            sb.Length -= 2;
            sb.Append($" offset {offset} rows fetch next {numberOfRecords} rows only");
            return sb.ToString();
        }
        #endregion PrivateMethods

    }



    public class DefaultQuery : Attribute
    {
        public string Query { get; set; }
        public DefaultQuery(string Query)
        {
            this.Query = Query;
        }
    }

    public class PageListObject<T>
    {
        public Pager pager { get; set; }
        public List<T> Results { get; set; }
    }

    public class PagedQuery
    {
        public string Query { get; set; }
        public string CountQuery { get; set; }
    }
    public class Pager
    {
        public int TotalRecords { get; set; }
        public int RecordsPerPage { get; set; }
        public int PageNumber { get; set; }
        public int Offset { get; set; }
        public int Fetch { get; set; }
        public int TotalPages { get; set; }
    }
    public class Schema : Attribute
    {
        public string SchemaName { get; set; }
        public Schema(string schemaName)
        {
            this.SchemaName = schemaName;
        }
    }
    public class Table : Attribute
    {
        public string TableName { get; set; }
        public Table(string tableName)
        {
            this.TableName = tableName;
        }
    }
    public class Column : Attribute
    {
        public string Name { get; set; }
        public Column(string name)
        {
            this.Name = name;
        }
    }
    public class DontUpdate : Attribute
    {
    }
    public class DontLoad : Attribute
    {
    }
    public class DontInsert : Attribute
    {
    }

    public class Key : Attribute
    {

    }

}

