﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CrowdFunding.Models.DBModels
{
    [Schema("Others")]
    [Table("Country")]
    public class Country
    {
        [DontInsert]
        [DontUpdate]
        public int ID { get; set; }
        public string FullName { get; set; }
        public string ShortName { get; set; }
    }
}