﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CrowdFunding.Models.DBModels
{
    [Schema("Projects")]
    [Table("Project")]
    public class Project
    {
        [Key]
        public int ID { get; set; }
        public int UserID { get; set; }
        public string Title { get; set; }
        [AllowHtml]
        public string Description { get; set; }
        public DateTime? FundedDate { get; set; }
        public string Rate { get; set; }
        public double Amount { get; set; }
        public bool Approved { get; set; }
        public string ImagePath { get; set; }
        public double Goal { get; set; }
        public DateTime TimeLeft { get; set; }


        public Project()
        {
            this.TimeLeft = DateTime.Now.AddDays(30);
        }
    }
}