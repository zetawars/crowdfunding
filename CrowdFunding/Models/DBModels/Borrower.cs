﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CrowdFunding.Models.DBModels
{
    [Schema("Users")]
    [Table("Borrower")]
    public class Borrower
    {
        public int ID { get; set; }
        public string CompanyID { get; set; }
        public string AmountTobeFunded { get; set; }
        public string DurationOfFunding { get; set; }
        public string Turnover { get; set; }
        public string PriorityOfLoan { get; set; }
    }
}