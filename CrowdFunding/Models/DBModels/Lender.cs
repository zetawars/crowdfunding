﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CrowdFunding.Models.DBModels
{
    [Schema("Users")]
    [Table("Lender")]
    public class Lender
    {
        public int ID { get; set; }
        public string Sponsor { get; set; }
        public int CountryID { get; set; }

    }
}