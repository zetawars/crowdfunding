﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CrowdFunding.Models.DBModels
{
    public enum DefaultRole
    {
        Admin = 1,
        Lender = 2,
        Borrower = 3
    }



    [Schema("Users")]
    [Table("Role")]
    public class Role
    {
        public int ID { get; set; }
        public string Name { get; set; }

        public Role AdminRole()
        {
            return new Role
            {
                ID = (int)DefaultRole.Admin,
                Name = DefaultRole.Admin.ToString()
            };
        }

        public Role LenderRole()
        {
            return new Role
            {
                ID = (int)DefaultRole.Lender,
                Name = DefaultRole.Lender.ToString()
            };
        }
        public Role BorrowerRole()
        {
            return new Role
            {
                ID = (int)DefaultRole.Borrower,
                Name = DefaultRole.Borrower.ToString()
            };
        }
    }
}