﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CrowdFunding.Models.DBModels
{
    [Schema("Projects")]
    [Table("Company")]
    public class Company
    {
        public int ID { get; set; }
        public string Title { get; set; }
        public string ImagePath { get; set; }
        public string Description { get; set; }
        public string Activity { get; set; }
        public string NIF { get; set; }
        public string NACECode { get; set; }
        public string ShareCapital { get; set; }
        public int YearOfCreation { get; set; }
        public string Employees { get; set; }
        public string CEO { get; set; }
        public string InCompanySince { get; set; }
        public string Website { get; set; }
        public string AnalystOpinion { get; set; }
        public double Lat { get; set; }
        public double Lng { get; set; }
        public string Profitability { get; set; }
        public string FinancialHealth { get; set; }
        public string CompanyHistory { get; set; }
    }
}