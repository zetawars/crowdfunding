﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CrowdFunding.Models.DBModels
{

    [Schema("Users")]
    [Table("User")]
    public class User
    {
        [DontInsert]
        [DontUpdate]
        public int ID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public int RoleID { get; set; }
        public bool IsDeleted { get; set; }
        public string MobileNo { get; set; }
        [DontInsert]
        [DontUpdate]
        public DateTime TimeStamp { get; set; }

        public string Address { get; set; }
        public string ImagePath { get; set; }


    }
}