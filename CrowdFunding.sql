USE [master]
GO
/****** Object:  Database [CrowdFunding]    Script Date: 2/10/2019 9:41:50 PM ******/
CREATE DATABASE [CrowdFunding]
 CONTAINMENT = NONE

ALTER DATABASE [CrowdFunding] SET COMPATIBILITY_LEVEL = 100
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [CrowdFunding].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [CrowdFunding] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [CrowdFunding] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [CrowdFunding] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [CrowdFunding] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [CrowdFunding] SET ARITHABORT OFF 
GO
ALTER DATABASE [CrowdFunding] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [CrowdFunding] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [CrowdFunding] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [CrowdFunding] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [CrowdFunding] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [CrowdFunding] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [CrowdFunding] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [CrowdFunding] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [CrowdFunding] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [CrowdFunding] SET  DISABLE_BROKER 
GO
ALTER DATABASE [CrowdFunding] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [CrowdFunding] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [CrowdFunding] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [CrowdFunding] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [CrowdFunding] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [CrowdFunding] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [CrowdFunding] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [CrowdFunding] SET RECOVERY FULL 
GO
ALTER DATABASE [CrowdFunding] SET  MULTI_USER 
GO
ALTER DATABASE [CrowdFunding] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [CrowdFunding] SET DB_CHAINING OFF 
GO
ALTER DATABASE [CrowdFunding] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [CrowdFunding] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [CrowdFunding] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [CrowdFunding] SET QUERY_STORE = OFF
GO
USE [CrowdFunding]
GO
ALTER DATABASE SCOPED CONFIGURATION SET LEGACY_CARDINALITY_ESTIMATION = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET LEGACY_CARDINALITY_ESTIMATION = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET MAXDOP = 0;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET MAXDOP = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET PARAMETER_SNIFFING = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET PARAMETER_SNIFFING = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET QUERY_OPTIMIZER_HOTFIXES = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET QUERY_OPTIMIZER_HOTFIXES = PRIMARY;
GO
USE [CrowdFunding]
GO
/****** Object:  Schema [Others]    Script Date: 2/10/2019 9:41:50 PM ******/
CREATE SCHEMA [Others]
GO
/****** Object:  Schema [Projects]    Script Date: 2/10/2019 9:41:50 PM ******/
CREATE SCHEMA [Projects]
GO
/****** Object:  Schema [Users]    Script Date: 2/10/2019 9:41:50 PM ******/
CREATE SCHEMA [Users]
GO
/****** Object:  UserDefinedFunction [dbo].[AdminID]    Script Date: 2/10/2019 9:41:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [dbo].[AdminID]()
RETURNS  INT
AS
BEGIN
 DECLARE @ID INT 
 SELECT @ID=ID FROM Users.[Role] Where Name = 'Admin'
 RETURN @ID
END
GO
/****** Object:  UserDefinedFunction [dbo].[DEFAULTDATE]    Script Date: 2/10/2019 9:41:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [dbo].[DEFAULTDATE]()

RETURNS  DATETIME
AS
BEGIN
 RETURN GETDATE()
END
GO
/****** Object:  UserDefinedFunction [dbo].[LenderID]    Script Date: 2/10/2019 9:41:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [dbo].[LenderID]()
RETURNS  INT
AS
BEGIN
 DECLARE @ID INT 
 SELECT @ID=ID FROM Users.[Role] Where Name = 'Lender'
 RETURN @ID
END
GO
/****** Object:  Table [Others].[Country]    Script Date: 2/10/2019 9:41:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Others].[Country](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[FullName] [varchar](50) NOT NULL,
	[ShortName] [varchar](10) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [Projects].[Company]    Script Date: 2/10/2019 9:41:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Projects].[Company](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](100) NULL,
	[ImagePath] [nvarchar](max) NULL,
	[Description] [nvarchar](max) NULL,
	[Activity] [varchar](50) NULL,
	[NIF] [varchar](100) NULL,
	[NACECode] [varchar](100) NULL,
	[ShareCapital] [varchar](100) NULL,
	[YearOfCreation] [varchar](100) NULL,
	[Employees] [varchar](100) NULL,
	[CEO] [varchar](100) NULL,
	[InCompanySince] [varchar](50) NULL,
	[Website] [varchar](max) NULL,
	[AnalystOpinion] [varchar](max) NULL,
	[Lat] [float] NULL,
	[Lng] [float] NULL,
	[Profitability] [varchar](2) NULL,
	[FinancialHealth] [varchar](2) NULL,
	[CompanyHistory] [varchar](2) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [Projects].[Project]    Script Date: 2/10/2019 9:41:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Projects].[Project](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Title] [varchar](100) NULL,
	[Description] [nvarchar](max) NULL,
	[FundedDate] [datetime] NULL,
	[Rate] [float] NOT NULL,
	[Amount] [float] NOT NULL,
	[ImagePath] [varchar](max) NULL,
	[Featured] [bit] NOT NULL,
	[Approved] [bit] NOT NULL,
	[ApprovedBy] [int] NULL,
	[UserID] [int] NOT NULL,
	[Goal] [float] NULL,
	[TimeLeft] [datetime] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
	[Popularity] [int] NOT NULL,
 CONSTRAINT [PK__Project__3214EC2792DAD217] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [Projects].[Project_Transactions]    Script Date: 2/10/2019 9:41:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Projects].[Project_Transactions](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ProjectID] [int] NOT NULL,
	[LenderID] [int] NOT NULL,
	[TransferedAmount] [float] NOT NULL,
	[TimeStamp] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [Projects].[ProjectImages]    Script Date: 2/10/2019 9:41:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Projects].[ProjectImages](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ProjectID] [int] NOT NULL,
	[ImagePath] [nvarchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [Projects].[ProjectTags]    Script Date: 2/10/2019 9:41:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Projects].[ProjectTags](
	[ProjectID] [int] NOT NULL,
	[TagID] [int] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [Projects].[Tags]    Script Date: 2/10/2019 9:41:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Projects].[Tags](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [Users].[ActivationCode]    Script Date: 2/10/2019 9:41:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Users].[ActivationCode](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](10) NULL,
	[Expires] [datetime] NOT NULL,
	[TimeStamp] [datetime] NOT NULL,
	[UserID] [int] NOT NULL,
	[Type] [varchar](50) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [Users].[Borrower]    Script Date: 2/10/2019 9:41:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Users].[Borrower](
	[ID] [int] NOT NULL,
	[CompanyID] [varchar](200) NULL,
	[AmountTobeFunded] [varchar](200) NULL,
	[DurationOfFunding] [varchar](200) NULL,
	[Turnover] [varchar](200) NULL,
	[PriorityOfLoan] [varchar](200) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [Users].[Lender]    Script Date: 2/10/2019 9:41:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Users].[Lender](
	[ID] [int] NULL,
	[Sponsor] [varchar](50) NULL,
	[CountryID] [int] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [Users].[Role]    Script Date: 2/10/2019 9:41:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Users].[Role](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [Users].[User]    Script Date: 2/10/2019 9:41:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Users].[User](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[RoleID] [int] NOT NULL,
	[Email] [varchar](255) NOT NULL,
	[Password] [varchar](max) NOT NULL,
	[FirstName] [varchar](50) NOT NULL,
	[LastName] [varchar](50) NULL,
	[TimeStamp] [datetime] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
	[MobileNo] [varchar](50) NOT NULL,
	[Address] [varchar](max) NULL,
	[ImagePath] [varchar](max) NULL,
 CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [Users].[UserAccount]    Script Date: 2/10/2019 9:41:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Users].[UserAccount](
	[UserID] [int] NOT NULL,
	[Amount] [float] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[UserID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [Users].[UserVerification]    Script Date: 2/10/2019 9:41:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Users].[UserVerification](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[UserID] [int] NOT NULL,
	[EmailVerified] [bit] NOT NULL,
	[PhoneVerified] [bit] NOT NULL,
	[IdentityDocument] [varchar](max) NULL,
	[IdentityDocumentVerified] [bit] NOT NULL,
	[ProofOfResidency] [varchar](max) NULL,
	[ProofOfResidencyVerified] [bit] NOT NULL,
	[BankDocument] [varchar](max) NULL,
	[BankDocumentVerified] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET IDENTITY_INSERT [Others].[Country] ON 
GO
INSERT [Others].[Country] ([ID], [FullName], [ShortName]) VALUES (1, N'France', N'FRA')
GO
INSERT [Others].[Country] ([ID], [FullName], [ShortName]) VALUES (2, N'Italy', N'ITA')
GO
INSERT [Others].[Country] ([ID], [FullName], [ShortName]) VALUES (3, N'Spain', N'ESP')
GO
INSERT [Others].[Country] ([ID], [FullName], [ShortName]) VALUES (4, N'Rest of the world', N'Other')
GO
SET IDENTITY_INSERT [Others].[Country] OFF
GO
SET IDENTITY_INSERT [Projects].[Project] ON 
GO
INSERT [Projects].[Project] ([ID], [Title], [Description], [FundedDate], [Rate], [Amount], [ImagePath], [Featured], [Approved], [ApprovedBy], [UserID], [Goal], [TimeLeft], [IsDeleted], [Popularity]) VALUES (1, N'Project 1', N' <h5 class="color-navy-blue xs-mb-30">We are a group of 41 leaders from Ascension Leadership Academy. As a team we are committed to creating a world of Freedom, Love, and Transformation.</h5>
                                <p class="xs-content-description fundpress-content-description xs-mb-30">Why I am supporting Safelight''s cause... It took me a long time to admit outloud that I had been in an abusive relationship for roughly the last year an d half of my college career. I studied domestic violence/interpersonal <span class="color-navy-blue">partner violence during that time</span>, but continued to do what many people trapped in those relationships do... </p>
                                <p class="xs-content-description fundpress-content-description xs-mb-30">I"is it worth it?", "do you really think that''s what love is?", and yes, even when said "you study this stuff, you know what this is, why do you let yourself go through this?"</p>
                                <h3 class="bold color-green xs-post-title fundpress-post-title xs-mb-30">Our Mission:</h3>
                                <p class="xs-content-description fundpress-content-description xs-mb-30">To raise $85,000 by October 14, 2017, in support of the Coalition to Abolish Slavery and Trafficking Los Angeles (CAST LA) and it''s mission to build a future where Human Trafficking exists.</p>
                                <p class="xs-content-description fundpress-content-description xs-mb-40">NYC Marathon 2016 was the most awesome race of all to date. The day was beautiful, the crowd fantastic, and I had a great day in the Big Apple even after taking a spill at Mile 13. Now, seriously ~ who breaks an arm running a marathon? And who keeps running because ''broken'' didn''t cross her mind? That would be me. What a learning curve!</p>
                                <h3 class="bold color-green xs-post-title fundpress-post-title xs-mb-30">Our Vission:</h3>
                                <p class="xs-content-description fundpress-content-description xs-mb-0">Why I am supporting Safelight''s cause... <span class="color-green">It took me a long time to admit outloud that</span> I had been in an abusive relationship for roughly the last year an d half of my college career. I studied domestic violence/interpersonal partner violence during that time, but continued to do what many people trapped in those relationships do... I stayed; even when asked "why?", "is it worth it?", "do you really think that''s what love is?", and yes, even when said "you study this stuff, you know what this is, why do you let yourself go through this?"</p>

', NULL, 12, 1000, N'~/Images/Projects/Project_78285.195136575585.png', 0, 0, NULL, 78, 87000, CAST(N'2019-02-05T20:57:46.577' AS DateTime), 1, 0)
GO
INSERT [Projects].[Project] ([ID], [Title], [Description], [FundedDate], [Rate], [Amount], [ImagePath], [Featured], [Approved], [ApprovedBy], [UserID], [Goal], [TimeLeft], [IsDeleted], [Popularity]) VALUES (4, N'asdas;', NULL, NULL, 12, 1200, NULL, 0, 0, NULL, 82, 0, CAST(N'2019-03-11T12:00:00.000' AS DateTime), 1, 0)
GO
INSERT [Projects].[Project] ([ID], [Title], [Description], [FundedDate], [Rate], [Amount], [ImagePath], [Featured], [Approved], [ApprovedBy], [UserID], [Goal], [TimeLeft], [IsDeleted], [Popularity]) VALUES (5, N'Project 2', NULL, NULL, 120, 12, NULL, 0, 0, NULL, 82, 0, CAST(N'2019-03-11T12:00:00.000' AS DateTime), 1, 0)
GO
INSERT [Projects].[Project] ([ID], [Title], [Description], [FundedDate], [Rate], [Amount], [ImagePath], [Featured], [Approved], [ApprovedBy], [UserID], [Goal], [TimeLeft], [IsDeleted], [Popularity]) VALUES (6, N'Brilliant After All, A New Album by Rebecca: Help poor people', N'<p style="margin: 0px 0px 15px; padding: 0px; text-align: justify; font-family: ''Open Sans'', Arial, sans-serif; font-size: 14px; background-color: #ffffff;">"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."</p>
<h3 style="margin: 15px 0px; padding: 0px; font-size: 14px; font-family: ''Open Sans'', Arial, sans-serif; background-color: #ffffff;">Section 1.10.32 of "de Finibus Bonorum et Malorum", written by Cicero in 45 BC</h3>
<p style="margin: 0px 0px 15px; padding: 0px; text-align: justify; font-family: ''Open Sans'', Arial, sans-serif; font-size: 14px; background-color: #ffffff;">"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?"</p>
<p style="margin: 0px 0px 15px; padding: 0px; text-align: justify; font-family: ''Open Sans'', Arial, sans-serif; font-size: 14px; background-color: #ffffff;">"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."</p>
<h3 style="margin: 15px 0px; padding: 0px; font-size: 14px; font-family: ''Open Sans'', Arial, sans-serif; background-color: #ffffff;">Section 1.10.32 of "de Finibus Bonorum et Malorum", written by Cicero in 45 BC</h3>
<p style="margin: 0px 0px 15px; padding: 0px; text-align: justify; font-family: ''Open Sans'', Arial, sans-serif; font-size: 14px; background-color: #ffffff;">"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?"</p>
<p style="margin: 0px 0px 15px; padding: 0px; text-align: justify; font-family: ''Open Sans'', Arial, sans-serif; font-size: 14px; background-color: #ffffff;">"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."</p>
<h3 style="margin: 15px 0px; padding: 0px; font-size: 14px; font-family: ''Open Sans'', Arial, sans-serif; background-color: #ffffff;">Section 1.10.32 of "de Finibus Bonorum et Malorum", written by Cicero in 45 BC</h3>
<p style="margin: 0px 0px 15px; padding: 0px; text-align: justify; font-family: ''Open Sans'', Arial, sans-serif; font-size: 14px; background-color: #ffffff;">"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?"</p>
<p style="margin: 0px 0px 15px; padding: 0px; text-align: justify; font-family: ''Open Sans'', Arial, sans-serif; font-size: 14px; background-color: #ffffff;">"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."</p>
<h3 style="margin: 15px 0px; padding: 0px; font-size: 14px; font-family: ''Open Sans'', Arial, sans-serif; background-color: #ffffff;">Section 1.10.32 of "de Finibus Bonorum et Malorum", written by Cicero in 45 BC</h3>
<p style="margin: 0px 0px 15px; padding: 0px; text-align: justify; font-family: ''Open Sans'', Arial, sans-serif; font-size: 14px; background-color: #ffffff;">"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?"</p>', NULL, 12, 2000, NULL, 0, 0, NULL, 82, 0, CAST(N'2019-03-11T12:00:00.000' AS DateTime), 1, 0)
GO
INSERT [Projects].[Project] ([ID], [Title], [Description], [FundedDate], [Rate], [Amount], [ImagePath], [Featured], [Approved], [ApprovedBy], [UserID], [Goal], [TimeLeft], [IsDeleted], [Popularity]) VALUES (7, N'Water Colors: Kim Keever''s First-Ever Monograph', N'<h3 style="margin: 15px 0px; padding: 0px; font-size: 14px; font-family: ''Open Sans'', Arial, sans-serif; background-color: #ffffff;">The standard Lorem Ipsum passage, used since the 1500s</h3>
<p style="margin: 0px 0px 15px; padding: 0px; text-align: justify; font-family: ''Open Sans'', Arial, sans-serif; font-size: 14px; background-color: #ffffff;">"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."</p>
<h3 style="margin: 15px 0px; padding: 0px; font-size: 14px; font-family: ''Open Sans'', Arial, sans-serif; background-color: #ffffff;">Section 1.10.32 of "de Finibus Bonorum et Malorum", written by Cicero in 45 BC</h3>
<p style="margin: 0px 0px 15px; padding: 0px; text-align: justify; font-family: ''Open Sans'', Arial, sans-serif; font-size: 14px; background-color: #ffffff;">"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?"</p>
<h3 style="margin: 15px 0px; padding: 0px; font-size: 14px; font-family: ''Open Sans'', Arial, sans-serif; background-color: #ffffff;">1914 translation by H. Rackham</h3>
<p style="margin: 0px 0px 15px; padding: 0px; text-align: justify; font-family: ''Open Sans'', Arial, sans-serif; font-size: 14px; background-color: #ffffff;">"But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with a man who chooses to enjoy a pleasure that has no annoying consequences, or one who avoids a pain that produces no resultant pleasure?"</p>
<h3 style="margin: 15px 0px; padding: 0px; font-size: 14px; font-family: ''Open Sans'', Arial, sans-serif; background-color: #ffffff;">Section 1.10.33 of "de Finibus Bonorum et Malorum", written by Cicero in 45 BC</h3>
<p style="margin: 0px 0px 15px; padding: 0px; text-align: justify; font-family: ''Open Sans'', Arial, sans-serif; font-size: 14px; background-color: #ffffff;">"At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat."</p>
<h3 style="margin: 15px 0px; padding: 0px; font-size: 14px; font-family: ''Open Sans'', Arial, sans-serif; background-color: #ffffff;">1914 translation by H. Rackham</h3>
<p style="margin: 0px 0px 15px; padding: 0px; text-align: justify; font-family: ''Open Sans'', Arial, sans-serif; font-size: 14px; background-color: #ffffff;">"On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire, that they cannot foresee the pain and trouble that are bound to ensue; and equal blame belongs to those who fail in their duty through weakness of will, which is the same as saying through shrinking from toil and pain. These cases are perfectly simple and easy to distinguish. In a free hour, when our power of choice is untrammelled and when nothing prevents our being able to do what we like best, every pleasure is to be welcomed and every pain avoided. But in certain circumstances and owing to the claims of duty or the obligations of business it will frequently occur that pleasures have to be repudiated and annoyances accepted. The wise man therefore always holds in these matters to this principle of selection: he rejects pleasures to secure other greater pleasures, or else he endures pains to avoid worse pains."</p>', NULL, 20, 50000, NULL, 1, 1, 78, 82, 0, CAST(N'2019-03-11T12:00:00.000' AS DateTime), 0, 0)
GO
INSERT [Projects].[Project] ([ID], [Title], [Description], [FundedDate], [Rate], [Amount], [ImagePath], [Featured], [Approved], [ApprovedBy], [UserID], [Goal], [TimeLeft], [IsDeleted], [Popularity]) VALUES (8, N'Dress up Box Make-Believe inform Clothing for Girls', N'<h3 style="margin: 15px 0px; padding: 0px; font-size: 14px; font-family: ''Open Sans'', Arial, sans-serif; background-color: #ffffff;">The standard Lorem Ipsum passage, used since the 1500s</h3>
<p style="margin: 0px 0px 15px; padding: 0px; text-align: justify; font-family: ''Open Sans'', Arial, sans-serif; font-size: 14px; background-color: #ffffff;">"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."</p>
<h3 style="margin: 15px 0px; padding: 0px; font-size: 14px; font-family: ''Open Sans'', Arial, sans-serif; background-color: #ffffff;">Section 1.10.32 of "de Finibus Bonorum et Malorum", written by Cicero in 45 BC</h3>
<p style="margin: 0px 0px 15px; padding: 0px; text-align: justify; font-family: ''Open Sans'', Arial, sans-serif; font-size: 14px; background-color: #ffffff;">"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?"</p>
<h3 style="margin: 15px 0px; padding: 0px; font-size: 14px; font-family: ''Open Sans'', Arial, sans-serif; background-color: #ffffff;">1914 translation by H. Rackham</h3>
<p style="margin: 0px 0px 15px; padding: 0px; text-align: justify; font-family: ''Open Sans'', Arial, sans-serif; font-size: 14px; background-color: #ffffff;">"But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with a man who chooses to enjoy a pleasure that has no annoying consequences, or one who avoids a pain that produces no resultant pleasure?"</p>
<h3 style="margin: 15px 0px; padding: 0px; font-size: 14px; font-family: ''Open Sans'', Arial, sans-serif; background-color: #ffffff;">Section 1.10.33 of "de Finibus Bonorum et Malorum", written by Cicero in 45 BC</h3>
<p style="margin: 0px 0px 15px; padding: 0px; text-align: justify; font-family: ''Open Sans'', Arial, sans-serif; font-size: 14px; background-color: #ffffff;">"At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat."</p>
<h3 style="margin: 15px 0px; padding: 0px; font-size: 14px; font-family: ''Open Sans'', Arial, sans-serif; background-color: #ffffff;">1914 translation by H. Rackham</h3>
<p style="margin: 0px 0px 15px; padding: 0px; text-align: justify; font-family: ''Open Sans'', Arial, sans-serif; font-size: 14px; background-color: #ffffff;">"On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire, that they cannot foresee the pain and trouble that are bound to ensue; and equal blame belongs to those who fail in their duty through weakness of will, which is the same as saying through shrinking from toil and pain. These cases are perfectly simple and easy to distinguish. In a free hour, when our power of choice is untrammelled and when nothing prevents our being able to do what we like best, every pleasure is to be welcomed and every pain avoided. But in certain circumstances and owing to the claims of duty or the obligations of business it will frequently occur that pleasures have to be repudiated and annoyances accepted. The wise man therefore always holds in these matters to this principle of selection: he rejects pleasures to secure other greater pleasures, or else he endures pains to avoid worse pains."</p>', NULL, 40, 100000, NULL, 1, 1, 78, 82, 0, CAST(N'2019-03-11T12:00:00.000' AS DateTime), 0, 0)
GO
INSERT [Projects].[Project] ([ID], [Title], [Description], [FundedDate], [Rate], [Amount], [ImagePath], [Featured], [Approved], [ApprovedBy], [UserID], [Goal], [TimeLeft], [IsDeleted], [Popularity]) VALUES (9, N'Splash Drone 3 a Fully Waterproof Drone that floats', N'<h3 style="margin: 15px 0px; padding: 0px; font-size: 14px; font-family: ''Open Sans'', Arial, sans-serif; background-color: #ffffff;">The standard Lorem Ipsum passage, used since the 1500s</h3>
<p style="margin: 0px 0px 15px; padding: 0px; text-align: justify; font-family: ''Open Sans'', Arial, sans-serif; font-size: 14px; background-color: #ffffff;">"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."</p>
<h3 style="margin: 15px 0px; padding: 0px; font-size: 14px; font-family: ''Open Sans'', Arial, sans-serif; background-color: #ffffff;">Section 1.10.32 of "de Finibus Bonorum et Malorum", written by Cicero in 45 BC</h3>
<p style="margin: 0px 0px 15px; padding: 0px; text-align: justify; font-family: ''Open Sans'', Arial, sans-serif; font-size: 14px; background-color: #ffffff;">"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?"</p>
<h3 style="margin: 15px 0px; padding: 0px; font-size: 14px; font-family: ''Open Sans'', Arial, sans-serif; background-color: #ffffff;">1914 translation by H. Rackham</h3>
<p style="margin: 0px 0px 15px; padding: 0px; text-align: justify; font-family: ''Open Sans'', Arial, sans-serif; font-size: 14px; background-color: #ffffff;">"But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with a man who chooses to enjoy a pleasure that has no annoying consequences, or one who avoids a pain that produces no resultant pleasure?"</p>
<h3 style="margin: 15px 0px; padding: 0px; font-size: 14px; font-family: ''Open Sans'', Arial, sans-serif; background-color: #ffffff;">Section 1.10.33 of "de Finibus Bonorum et Malorum", written by Cicero in 45 BC</h3>
<p style="margin: 0px 0px 15px; padding: 0px; text-align: justify; font-family: ''Open Sans'', Arial, sans-serif; font-size: 14px; background-color: #ffffff;">"At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat."</p>
<h3 style="margin: 15px 0px; padding: 0px; font-size: 14px; font-family: ''Open Sans'', Arial, sans-serif; background-color: #ffffff;">1914 translation by H. Rackham</h3>
<p style="margin: 0px 0px 15px; padding: 0px; text-align: justify; font-family: ''Open Sans'', Arial, sans-serif; font-size: 14px; background-color: #ffffff;">"On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire, that they cannot foresee the pain and trouble that are bound to ensue; and equal blame belongs to those who fail in their duty through weakness of will, which is the same as saying through shrinking from toil and pain. These cases are perfectly simple and easy to distinguish. In a free hour, when our power of choice is untrammelled and when nothing prevents our being able to do what we like best, every pleasure is to be welcomed and every pain avoided. But in certain circumstances and owing to the claims of duty or the obligations of business it will frequently occur that pleasures have to be repudiated and annoyances accepted. The wise man therefore always holds in these matters to this principle of selection: he rejects pleasures to secure other greater pleasures, or else he endures pains to avoid worse pains."</p>', NULL, 50, 100000, NULL, 0, 1, 78, 82, 0, CAST(N'2019-03-11T12:00:00.000' AS DateTime), 0, 0)
GO
INSERT [Projects].[Project] ([ID], [Title], [Description], [FundedDate], [Rate], [Amount], [ImagePath], [Featured], [Approved], [ApprovedBy], [UserID], [Goal], [TimeLeft], [IsDeleted], [Popularity]) VALUES (10, N'The Read Read: Braille Literacy Tool for the Blind', N'<h3 style="margin: 15px 0px; padding: 0px; font-size: 14px; font-family: ''Open Sans'', Arial, sans-serif; background-color: #ffffff;">The standard Lorem Ipsum passage, used since the 1500s</h3>
<p style="margin: 0px 0px 15px; padding: 0px; text-align: justify; font-family: ''Open Sans'', Arial, sans-serif; font-size: 14px; background-color: #ffffff;">"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."</p>
<h3 style="margin: 15px 0px; padding: 0px; font-size: 14px; font-family: ''Open Sans'', Arial, sans-serif; background-color: #ffffff;">Section 1.10.32 of "de Finibus Bonorum et Malorum", written by Cicero in 45 BC</h3>
<p style="margin: 0px 0px 15px; padding: 0px; text-align: justify; font-family: ''Open Sans'', Arial, sans-serif; font-size: 14px; background-color: #ffffff;">"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?"</p>
<h3 style="margin: 15px 0px; padding: 0px; font-size: 14px; font-family: ''Open Sans'', Arial, sans-serif; background-color: #ffffff;">1914 translation by H. Rackham</h3>
<p style="margin: 0px 0px 15px; padding: 0px; text-align: justify; font-family: ''Open Sans'', Arial, sans-serif; font-size: 14px; background-color: #ffffff;">"But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with a man who chooses to enjoy a pleasure that has no annoying consequences, or one who avoids a pain that produces no resultant pleasure?"</p>
<h3 style="margin: 15px 0px; padding: 0px; font-size: 14px; font-family: ''Open Sans'', Arial, sans-serif; background-color: #ffffff;">Section 1.10.33 of "de Finibus Bonorum et Malorum", written by Cicero in 45 BC</h3>
<p style="margin: 0px 0px 15px; padding: 0px; text-align: justify; font-family: ''Open Sans'', Arial, sans-serif; font-size: 14px; background-color: #ffffff;">"At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat."</p>
<h3 style="margin: 15px 0px; padding: 0px; font-size: 14px; font-family: ''Open Sans'', Arial, sans-serif; background-color: #ffffff;">1914 translation by H. Rackham</h3>
<p style="margin: 0px 0px 15px; padding: 0px; text-align: justify; font-family: ''Open Sans'', Arial, sans-serif; font-size: 14px; background-color: #ffffff;">"On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire, that they cannot foresee the pain and trouble that are bound to ensue; and equal blame belongs to those who fail in their duty through weakness of will, which is the same as saying through shrinking from toil and pain. These cases are perfectly simple and easy to distinguish. In a free hour, when our power of choice is untrammelled and when nothing prevents our being able to do what we like best, every pleasure is to be welcomed and every pain avoided. But in certain circumstances and owing to the claims of duty or the obligations of business it will frequently occur that pleasures have to be repudiated and annoyances accepted. The wise man therefore always holds in these matters to this principle of selection: he rejects pleasures to secure other greater pleasures, or else he endures pains to avoid worse pains."</p>', NULL, 40, 60000, NULL, 0, 1, 78, 82, 0, CAST(N'2019-03-11T12:00:00.000' AS DateTime), 0, 0)
GO
INSERT [Projects].[Project] ([ID], [Title], [Description], [FundedDate], [Rate], [Amount], [ImagePath], [Featured], [Approved], [ApprovedBy], [UserID], [Goal], [TimeLeft], [IsDeleted], [Popularity]) VALUES (11, N'BuildOne: $99 3D Printer w/ WiFi and Auto Bed Leveling!', N'<h3 style="margin: 15px 0px; padding: 0px; font-size: 14px; font-family: ''Open Sans'', Arial, sans-serif; background-color: #ffffff;">The standard Lorem Ipsum passage, used since the 1500s</h3>
<p style="margin: 0px 0px 15px; padding: 0px; text-align: justify; font-family: ''Open Sans'', Arial, sans-serif; font-size: 14px; background-color: #ffffff;">"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."</p>
<h3 style="margin: 15px 0px; padding: 0px; font-size: 14px; font-family: ''Open Sans'', Arial, sans-serif; background-color: #ffffff;">Section 1.10.32 of "de Finibus Bonorum et Malorum", written by Cicero in 45 BC</h3>
<p style="margin: 0px 0px 15px; padding: 0px; text-align: justify; font-family: ''Open Sans'', Arial, sans-serif; font-size: 14px; background-color: #ffffff;">"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?"</p>
<h3 style="margin: 15px 0px; padding: 0px; font-size: 14px; font-family: ''Open Sans'', Arial, sans-serif; background-color: #ffffff;">1914 translation by H. Rackham</h3>
<p style="margin: 0px 0px 15px; padding: 0px; text-align: justify; font-family: ''Open Sans'', Arial, sans-serif; font-size: 14px; background-color: #ffffff;">"But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with a man who chooses to enjoy a pleasure that has no annoying consequences, or one who avoids a pain that produces no resultant pleasure?"</p>
<h3 style="margin: 15px 0px; padding: 0px; font-size: 14px; font-family: ''Open Sans'', Arial, sans-serif; background-color: #ffffff;">Section 1.10.33 of "de Finibus Bonorum et Malorum", written by Cicero in 45 BC</h3>
<p style="margin: 0px 0px 15px; padding: 0px; text-align: justify; font-family: ''Open Sans'', Arial, sans-serif; font-size: 14px; background-color: #ffffff;">"At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat."</p>
<h3 style="margin: 15px 0px; padding: 0px; font-size: 14px; font-family: ''Open Sans'', Arial, sans-serif; background-color: #ffffff;">1914 translation by H. Rackham</h3>
<p style="margin: 0px 0px 15px; padding: 0px; text-align: justify; font-family: ''Open Sans'', Arial, sans-serif; font-size: 14px; background-color: #ffffff;">"On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire, that they cannot foresee the pain and trouble that are bound to ensue; and equal blame belongs to those who fail in their duty through weakness of will, which is the same as saying through shrinking from toil and pain. These cases are perfectly simple and easy to distinguish. In a free hour, when our power of choice is untrammelled and when nothing prevents our being able to do what we like best, every pleasure is to be welcomed and every pain avoided. But in certain circumstances and owing to the claims of duty or the obligations of business it will frequently occur that pleasures have to be repudiated and annoyances accepted. The wise man therefore always holds in these matters to this principle of selection: he rejects pleasures to secure other greater pleasures, or else he endures pains to avoid worse pains."</p>', NULL, 40, 50000, NULL, 0, 1, 78, 82, 0, CAST(N'2019-03-11T12:00:00.000' AS DateTime), 0, 0)
GO
INSERT [Projects].[Project] ([ID], [Title], [Description], [FundedDate], [Rate], [Amount], [ImagePath], [Featured], [Approved], [ApprovedBy], [UserID], [Goal], [TimeLeft], [IsDeleted], [Popularity]) VALUES (12, N'DACBerry PRO  Professional Soundcard for Raspberry Pi', N'<h3 style="margin: 15px 0px; padding: 0px; font-size: 14px; font-family: ''Open Sans'', Arial, sans-serif; background-color: #ffffff;">The standard Lorem Ipsum passage, used since the 1500s</h3>
<p style="margin: 0px 0px 15px; padding: 0px; text-align: justify; font-family: ''Open Sans'', Arial, sans-serif; font-size: 14px; background-color: #ffffff;">"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."</p>
<h3 style="margin: 15px 0px; padding: 0px; font-size: 14px; font-family: ''Open Sans'', Arial, sans-serif; background-color: #ffffff;">Section 1.10.32 of "de Finibus Bonorum et Malorum", written by Cicero in 45 BC</h3>
<p style="margin: 0px 0px 15px; padding: 0px; text-align: justify; font-family: ''Open Sans'', Arial, sans-serif; font-size: 14px; background-color: #ffffff;">"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?"</p>
<h3 style="margin: 15px 0px; padding: 0px; font-size: 14px; font-family: ''Open Sans'', Arial, sans-serif; background-color: #ffffff;">1914 translation by H. Rackham</h3>
<p style="margin: 0px 0px 15px; padding: 0px; text-align: justify; font-family: ''Open Sans'', Arial, sans-serif; font-size: 14px; background-color: #ffffff;">"But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with a man who chooses to enjoy a pleasure that has no annoying consequences, or one who avoids a pain that produces no resultant pleasure?"</p>
<h3 style="margin: 15px 0px; padding: 0px; font-size: 14px; font-family: ''Open Sans'', Arial, sans-serif; background-color: #ffffff;">Section 1.10.33 of "de Finibus Bonorum et Malorum", written by Cicero in 45 BC</h3>
<p style="margin: 0px 0px 15px; padding: 0px; text-align: justify; font-family: ''Open Sans'', Arial, sans-serif; font-size: 14px; background-color: #ffffff;">"At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat."</p>
<h3 style="margin: 15px 0px; padding: 0px; font-size: 14px; font-family: ''Open Sans'', Arial, sans-serif; background-color: #ffffff;">1914 translation by H. Rackham</h3>
<p style="margin: 0px 0px 15px; padding: 0px; text-align: justify; font-family: ''Open Sans'', Arial, sans-serif; font-size: 14px; background-color: #ffffff;">"On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire, that they cannot foresee the pain and trouble that are bound to ensue; and equal blame belongs to those who fail in their duty through weakness of will, which is the same as saying through shrinking from toil and pain. These cases are perfectly simple and easy to distinguish. In a free hour, when our power of choice is untrammelled and when nothing prevents our being able to do what we like best, every pleasure is to be welcomed and every pain avoided. But in certain circumstances and owing to the claims of duty or the obligations of business it will frequently occur that pleasures have to be repudiated and annoyances accepted. The wise man therefore always holds in these matters to this principle of selection: he rejects pleasures to secure other greater pleasures, or else he endures pains to avoid worse pains."</p>', NULL, 40, 120000, NULL, 0, 1, 78, 82, 0, CAST(N'2019-03-11T12:00:00.000' AS DateTime), 0, 0)
GO
INSERT [Projects].[Project] ([ID], [Title], [Description], [FundedDate], [Rate], [Amount], [ImagePath], [Featured], [Approved], [ApprovedBy], [UserID], [Goal], [TimeLeft], [IsDeleted], [Popularity]) VALUES (13, N'BIKI: First Bionic Wireless Under water Fish Drone', N'<h3 style="margin: 15px 0px; padding: 0px; font-size: 14px; font-family: ''Open Sans'', Arial, sans-serif; background-color: #ffffff;">The standard Lorem Ipsum passage, used since the 1500s</h3>
<p style="margin: 0px 0px 15px; padding: 0px; text-align: justify; font-family: ''Open Sans'', Arial, sans-serif; font-size: 14px; background-color: #ffffff;">"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."</p>
<h3 style="margin: 15px 0px; padding: 0px; font-size: 14px; font-family: ''Open Sans'', Arial, sans-serif; background-color: #ffffff;">Section 1.10.32 of "de Finibus Bonorum et Malorum", written by Cicero in 45 BC</h3>
<p style="margin: 0px 0px 15px; padding: 0px; text-align: justify; font-family: ''Open Sans'', Arial, sans-serif; font-size: 14px; background-color: #ffffff;">"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?"</p>
<h3 style="margin: 15px 0px; padding: 0px; font-size: 14px; font-family: ''Open Sans'', Arial, sans-serif; background-color: #ffffff;">1914 translation by H. Rackham</h3>
<p style="margin: 0px 0px 15px; padding: 0px; text-align: justify; font-family: ''Open Sans'', Arial, sans-serif; font-size: 14px; background-color: #ffffff;">"But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with a man who chooses to enjoy a pleasure that has no annoying consequences, or one who avoids a pain that produces no resultant pleasure?"</p>
<h3 style="margin: 15px 0px; padding: 0px; font-size: 14px; font-family: ''Open Sans'', Arial, sans-serif; background-color: #ffffff;">Section 1.10.33 of "de Finibus Bonorum et Malorum", written by Cicero in 45 BC</h3>
<p style="margin: 0px 0px 15px; padding: 0px; text-align: justify; font-family: ''Open Sans'', Arial, sans-serif; font-size: 14px; background-color: #ffffff;">"At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat."</p>
<h3 style="margin: 15px 0px; padding: 0px; font-size: 14px; font-family: ''Open Sans'', Arial, sans-serif; background-color: #ffffff;">1914 translation by H. Rackham</h3>
<p style="margin: 0px 0px 15px; padding: 0px; text-align: justify; font-family: ''Open Sans'', Arial, sans-serif; font-size: 14px; background-color: #ffffff;">"On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire, that they cannot foresee the pain and trouble that are bound to ensue; and equal blame belongs to those who fail in their duty through weakness of will, which is the same as saying through shrinking from toil and pain. These cases are perfectly simple and easy to distinguish. In a free hour, when our power of choice is untrammelled and when nothing prevents our being able to do what we like best, every pleasure is to be welcomed and every pain avoided. But in certain circumstances and owing to the claims of duty or the obligations of business it will frequently occur that pleasures have to be repudiated and annoyances accepted. The wise man therefore always holds in these matters to this principle of selection: he rejects pleasures to secure other greater pleasures, or else he endures pains to avoid worse pains."</p>', NULL, 40, 60000, NULL, 0, 1, 78, 82, 0, CAST(N'2019-03-11T12:00:00.000' AS DateTime), 0, 0)
GO
INSERT [Projects].[Project] ([ID], [Title], [Description], [FundedDate], [Rate], [Amount], [ImagePath], [Featured], [Approved], [ApprovedBy], [UserID], [Goal], [TimeLeft], [IsDeleted], [Popularity]) VALUES (14, N'Brilliant After All, A New Album by Rebecca: Help poor people', N'<h3 style="margin: 15px 0px; padding: 0px; font-size: 14px; font-family: ''Open Sans'', Arial, sans-serif; background-color: #ffffff;">The standard Lorem Ipsum passage, used since the 1500s</h3>
<p style="margin: 0px 0px 15px; padding: 0px; text-align: justify; font-family: ''Open Sans'', Arial, sans-serif; font-size: 14px; background-color: #ffffff;">"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."</p>
<h3 style="margin: 15px 0px; padding: 0px; font-size: 14px; font-family: ''Open Sans'', Arial, sans-serif; background-color: #ffffff;">Section 1.10.32 of "de Finibus Bonorum et Malorum", written by Cicero in 45 BC</h3>
<p style="margin: 0px 0px 15px; padding: 0px; text-align: justify; font-family: ''Open Sans'', Arial, sans-serif; font-size: 14px; background-color: #ffffff;">"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?"</p>
<h3 style="margin: 15px 0px; padding: 0px; font-size: 14px; font-family: ''Open Sans'', Arial, sans-serif; background-color: #ffffff;">1914 translation by H. Rackham</h3>
<p style="margin: 0px 0px 15px; padding: 0px; text-align: justify; font-family: ''Open Sans'', Arial, sans-serif; font-size: 14px; background-color: #ffffff;">"But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with a man who chooses to enjoy a pleasure that has no annoying consequences, or one who avoids a pain that produces no resultant pleasure?"</p>
<h3 style="margin: 15px 0px; padding: 0px; font-size: 14px; font-family: ''Open Sans'', Arial, sans-serif; background-color: #ffffff;">Section 1.10.33 of "de Finibus Bonorum et Malorum", written by Cicero in 45 BC</h3>
<p style="margin: 0px 0px 15px; padding: 0px; text-align: justify; font-family: ''Open Sans'', Arial, sans-serif; font-size: 14px; background-color: #ffffff;">"At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat."</p>
<h3 style="margin: 15px 0px; padding: 0px; font-size: 14px; font-family: ''Open Sans'', Arial, sans-serif; background-color: #ffffff;">1914 translation by H. Rackham</h3>
<p style="margin: 0px 0px 15px; padding: 0px; text-align: justify; font-family: ''Open Sans'', Arial, sans-serif; font-size: 14px; background-color: #ffffff;">"On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire, that they cannot foresee the pain and trouble that are bound to ensue; and equal blame belongs to those who fail in their duty through weakness of will, which is the same as saying through shrinking from toil and pain. These cases are perfectly simple and easy to distinguish. In a free hour, when our power of choice is untrammelled and when nothing prevents our being able to do what we like best, every pleasure is to be welcomed and every pain avoided. But in certain circumstances and owing to the claims of duty or the obligations of business it will frequently occur that pleasures have to be repudiated and annoyances accepted. The wise man therefore always holds in these matters to this principle of selection: he rejects pleasures to secure other greater pleasures, or else he endures pains to avoid worse pains."</p>', NULL, 50, 120000, NULL, 1, 1, 78, 82, 0, CAST(N'2019-03-11T12:00:00.000' AS DateTime), 0, 0)
GO
SET IDENTITY_INSERT [Projects].[Project] OFF
GO
SET IDENTITY_INSERT [Projects].[Project_Transactions] ON 
GO
INSERT [Projects].[Project_Transactions] ([ID], [ProjectID], [LenderID], [TransferedAmount], [TimeStamp]) VALUES (1, 1, 79, 1000, CAST(N'2019-02-10T21:18:40.747' AS DateTime))
GO
INSERT [Projects].[Project_Transactions] ([ID], [ProjectID], [LenderID], [TransferedAmount], [TimeStamp]) VALUES (2, 1, 79, 1000, CAST(N'2019-02-10T21:18:40.747' AS DateTime))
GO
SET IDENTITY_INSERT [Projects].[Project_Transactions] OFF
GO
SET IDENTITY_INSERT [Projects].[ProjectImages] ON 
GO
INSERT [Projects].[ProjectImages] ([ID], [ProjectID], [ImagePath]) VALUES (1, 1, N'~/Assets/FrontEnd/assets/images/product/product_1.jpg')
GO
INSERT [Projects].[ProjectImages] ([ID], [ProjectID], [ImagePath]) VALUES (2, 1, N'~/Assets/FrontEnd/assets/images/product/product_2.jpg')
GO
INSERT [Projects].[ProjectImages] ([ID], [ProjectID], [ImagePath]) VALUES (3, 1, N'~/Assets/FrontEnd/assets/images/product/product_3.jpg')
GO
INSERT [Projects].[ProjectImages] ([ID], [ProjectID], [ImagePath]) VALUES (4, 1, N'~/Assets/FrontEnd/assets/images/product/product_4.jpg')
GO
INSERT [Projects].[ProjectImages] ([ID], [ProjectID], [ImagePath]) VALUES (26, 6, N'~/Images/Projects/Project_8225.6623514116101.jpg')
GO
INSERT [Projects].[ProjectImages] ([ID], [ProjectID], [ImagePath]) VALUES (27, 7, N'~/Images/Projects/Project_82575.267201091753.jpg')
GO
INSERT [Projects].[ProjectImages] ([ID], [ProjectID], [ImagePath]) VALUES (28, 8, N'~/Images/Projects/Project_82811.16656018941.jpg')
GO
INSERT [Projects].[ProjectImages] ([ID], [ProjectID], [ImagePath]) VALUES (29, 9, N'~/Images/Projects/Project_82376.332841988808.jpg')
GO
INSERT [Projects].[ProjectImages] ([ID], [ProjectID], [ImagePath]) VALUES (30, 10, N'~/Images/Projects/Project_82471.569157425114.jpg')
GO
INSERT [Projects].[ProjectImages] ([ID], [ProjectID], [ImagePath]) VALUES (31, 11, N'~/Images/Projects/Project_82882.652737611277.jpg')
GO
INSERT [Projects].[ProjectImages] ([ID], [ProjectID], [ImagePath]) VALUES (32, 12, N'~/Images/Projects/Project_82886.880058276877.jpg')
GO
INSERT [Projects].[ProjectImages] ([ID], [ProjectID], [ImagePath]) VALUES (33, 13, N'~/Images/Projects/Project_8212.0599535350036.jpg')
GO
INSERT [Projects].[ProjectImages] ([ID], [ProjectID], [ImagePath]) VALUES (34, 14, N'~/Images/Projects/Project_82789.169748215549.jpg')
GO
SET IDENTITY_INSERT [Projects].[ProjectImages] OFF
GO
INSERT [Projects].[ProjectTags] ([ProjectID], [TagID]) VALUES (1, 1)
GO
INSERT [Projects].[ProjectTags] ([ProjectID], [TagID]) VALUES (1, 2)
GO
INSERT [Projects].[ProjectTags] ([ProjectID], [TagID]) VALUES (1, 3)
GO
INSERT [Projects].[ProjectTags] ([ProjectID], [TagID]) VALUES (1, 4)
GO
INSERT [Projects].[ProjectTags] ([ProjectID], [TagID]) VALUES (1, 5)
GO
INSERT [Projects].[ProjectTags] ([ProjectID], [TagID]) VALUES (1, 6)
GO
INSERT [Projects].[ProjectTags] ([ProjectID], [TagID]) VALUES (1, 7)
GO
INSERT [Projects].[ProjectTags] ([ProjectID], [TagID]) VALUES (1, 8)
GO
INSERT [Projects].[ProjectTags] ([ProjectID], [TagID]) VALUES (1, 9)
GO
INSERT [Projects].[ProjectTags] ([ProjectID], [TagID]) VALUES (1, 10)
GO
INSERT [Projects].[ProjectTags] ([ProjectID], [TagID]) VALUES (4, 2)
GO
INSERT [Projects].[ProjectTags] ([ProjectID], [TagID]) VALUES (4, 4)
GO
INSERT [Projects].[ProjectTags] ([ProjectID], [TagID]) VALUES (4, 5)
GO
INSERT [Projects].[ProjectTags] ([ProjectID], [TagID]) VALUES (4, 6)
GO
INSERT [Projects].[ProjectTags] ([ProjectID], [TagID]) VALUES (4, 8)
GO
INSERT [Projects].[ProjectTags] ([ProjectID], [TagID]) VALUES (6, 10)
GO
INSERT [Projects].[ProjectTags] ([ProjectID], [TagID]) VALUES (6, 11)
GO
INSERT [Projects].[ProjectTags] ([ProjectID], [TagID]) VALUES (6, 12)
GO
INSERT [Projects].[ProjectTags] ([ProjectID], [TagID]) VALUES (7, 2)
GO
INSERT [Projects].[ProjectTags] ([ProjectID], [TagID]) VALUES (7, 13)
GO
INSERT [Projects].[ProjectTags] ([ProjectID], [TagID]) VALUES (7, 14)
GO
INSERT [Projects].[ProjectTags] ([ProjectID], [TagID]) VALUES (8, 7)
GO
INSERT [Projects].[ProjectTags] ([ProjectID], [TagID]) VALUES (8, 14)
GO
INSERT [Projects].[ProjectTags] ([ProjectID], [TagID]) VALUES (8, 15)
GO
INSERT [Projects].[ProjectTags] ([ProjectID], [TagID]) VALUES (9, 12)
GO
INSERT [Projects].[ProjectTags] ([ProjectID], [TagID]) VALUES (10, 16)
GO
INSERT [Projects].[ProjectTags] ([ProjectID], [TagID]) VALUES (11, 16)
GO
INSERT [Projects].[ProjectTags] ([ProjectID], [TagID]) VALUES (11, 17)
GO
INSERT [Projects].[ProjectTags] ([ProjectID], [TagID]) VALUES (12, 7)
GO
INSERT [Projects].[ProjectTags] ([ProjectID], [TagID]) VALUES (12, 12)
GO
INSERT [Projects].[ProjectTags] ([ProjectID], [TagID]) VALUES (13, 16)
GO
INSERT [Projects].[ProjectTags] ([ProjectID], [TagID]) VALUES (14, 16)
GO
SET IDENTITY_INSERT [Projects].[Tags] ON 
GO
INSERT [Projects].[Tags] ([ID], [Name]) VALUES (1, N'Comics')
GO
INSERT [Projects].[Tags] ([ID], [Name]) VALUES (2, N'Crafts')
GO
INSERT [Projects].[Tags] ([ID], [Name]) VALUES (3, N'Dance')
GO
INSERT [Projects].[Tags] ([ID], [Name]) VALUES (4, N'Design')
GO
INSERT [Projects].[Tags] ([ID], [Name]) VALUES (5, N'Fashion')
GO
INSERT [Projects].[Tags] ([ID], [Name]) VALUES (6, N'Food')
GO
INSERT [Projects].[Tags] ([ID], [Name]) VALUES (7, N'Games')
GO
INSERT [Projects].[Tags] ([ID], [Name]) VALUES (8, N'Film And Video')
GO
INSERT [Projects].[Tags] ([ID], [Name]) VALUES (9, N'Journalism')
GO
INSERT [Projects].[Tags] ([ID], [Name]) VALUES (10, N'Music')
GO
INSERT [Projects].[Tags] ([ID], [Name]) VALUES (11, N'UI/UX')
GO
INSERT [Projects].[Tags] ([ID], [Name]) VALUES (12, N'Technology')
GO
INSERT [Projects].[Tags] ([ID], [Name]) VALUES (13, N'Homemade')
GO
INSERT [Projects].[Tags] ([ID], [Name]) VALUES (14, N'Photography')
GO
INSERT [Projects].[Tags] ([ID], [Name]) VALUES (15, N'Lifestyle')
GO
INSERT [Projects].[Tags] ([ID], [Name]) VALUES (16, N'Charity')
GO
INSERT [Projects].[Tags] ([ID], [Name]) VALUES (17, N'Funding')
GO
SET IDENTITY_INSERT [Projects].[Tags] OFF
GO
SET IDENTITY_INSERT [Users].[ActivationCode] ON 
GO
INSERT [Users].[ActivationCode] ([ID], [Code], [Expires], [TimeStamp], [UserID], [Type]) VALUES (44, N'4148', CAST(N'2019-01-17T16:01:30.090' AS DateTime), CAST(N'2019-01-15T16:01:30.090' AS DateTime), 79, N'Email')
GO
INSERT [Users].[ActivationCode] ([ID], [Code], [Expires], [TimeStamp], [UserID], [Type]) VALUES (45, N'8365', CAST(N'2019-01-17T16:01:30.090' AS DateTime), CAST(N'2019-01-15T16:01:30.090' AS DateTime), 79, N'Phone')
GO
INSERT [Users].[ActivationCode] ([ID], [Code], [Expires], [TimeStamp], [UserID], [Type]) VALUES (46, N'1466', CAST(N'2019-02-06T22:19:26.067' AS DateTime), CAST(N'2019-02-04T22:19:26.067' AS DateTime), 80, N'Email')
GO
INSERT [Users].[ActivationCode] ([ID], [Code], [Expires], [TimeStamp], [UserID], [Type]) VALUES (47, N'1491', CAST(N'2019-02-06T22:19:26.067' AS DateTime), CAST(N'2019-02-04T22:19:26.067' AS DateTime), 80, N'Phone')
GO
INSERT [Users].[ActivationCode] ([ID], [Code], [Expires], [TimeStamp], [UserID], [Type]) VALUES (48, N'1565', CAST(N'2019-02-06T22:21:07.953' AS DateTime), CAST(N'2019-02-04T22:21:07.953' AS DateTime), 81, N'Email')
GO
INSERT [Users].[ActivationCode] ([ID], [Code], [Expires], [TimeStamp], [UserID], [Type]) VALUES (49, N'9675', CAST(N'2019-02-06T22:21:07.953' AS DateTime), CAST(N'2019-02-04T22:21:07.953' AS DateTime), 81, N'Phone')
GO
INSERT [Users].[ActivationCode] ([ID], [Code], [Expires], [TimeStamp], [UserID], [Type]) VALUES (50, N'5371', CAST(N'2019-02-06T22:25:28.090' AS DateTime), CAST(N'2019-02-04T22:25:28.090' AS DateTime), 82, N'Email')
GO
INSERT [Users].[ActivationCode] ([ID], [Code], [Expires], [TimeStamp], [UserID], [Type]) VALUES (51, N'8753', CAST(N'2019-02-06T22:25:28.090' AS DateTime), CAST(N'2019-02-04T22:25:28.090' AS DateTime), 82, N'Phone')
GO
SET IDENTITY_INSERT [Users].[ActivationCode] OFF
GO
INSERT [Users].[Borrower] ([ID], [CompanyID], [AmountTobeFunded], [DurationOfFunding], [Turnover], [PriorityOfLoan]) VALUES (43, N'453798', N'', N'30', N'Less than  250,000', N'Urgent')
GO
INSERT [Users].[Borrower] ([ID], [CompanyID], [AmountTobeFunded], [DurationOfFunding], [Turnover], [PriorityOfLoan]) VALUES (61, N'453798', N'7879', N'30', N'Less than  250,000', N'Urgent')
GO
INSERT [Users].[Borrower] ([ID], [CompanyID], [AmountTobeFunded], [DurationOfFunding], [Turnover], [PriorityOfLoan]) VALUES (62, N'890987698', N'7879', N'30', N'Less than  250,000', N'Urgent')
GO
INSERT [Users].[Borrower] ([ID], [CompanyID], [AmountTobeFunded], [DurationOfFunding], [Turnover], [PriorityOfLoan]) VALUES (63, N'8796768', N'76876876', N'86', N'Less than  250,000', N'In a few weeks')
GO
INSERT [Users].[Borrower] ([ID], [CompanyID], [AmountTobeFunded], [DurationOfFunding], [Turnover], [PriorityOfLoan]) VALUES (64, N'8796768', N'76876876', N'86', N'Less than  250,000', N'In a few weeks')
GO
INSERT [Users].[Borrower] ([ID], [CompanyID], [AmountTobeFunded], [DurationOfFunding], [Turnover], [PriorityOfLoan]) VALUES (65, N'8796768', N'76876876', N'86', N'Less than  250,000', N'In a few weeks')
GO
INSERT [Users].[Borrower] ([ID], [CompanyID], [AmountTobeFunded], [DurationOfFunding], [Turnover], [PriorityOfLoan]) VALUES (66, N'8796768', N'76876876', N'86', N'Less than  250,000', N'In a few weeks')
GO
INSERT [Users].[Borrower] ([ID], [CompanyID], [AmountTobeFunded], [DurationOfFunding], [Turnover], [PriorityOfLoan]) VALUES (67, N'8796768', N'76876876', N'86', N'Less than  250,000', N'In a few weeks')
GO
INSERT [Users].[Borrower] ([ID], [CompanyID], [AmountTobeFunded], [DurationOfFunding], [Turnover], [PriorityOfLoan]) VALUES (68, N'8796768', N'76876876', N'86', N'Less than  250,000', N'In a few weeks')
GO
INSERT [Users].[Borrower] ([ID], [CompanyID], [AmountTobeFunded], [DurationOfFunding], [Turnover], [PriorityOfLoan]) VALUES (69, N'8796768', N'76876876', N'86', N'Less than  250,000', N'In a few weeks')
GO
INSERT [Users].[Borrower] ([ID], [CompanyID], [AmountTobeFunded], [DurationOfFunding], [Turnover], [PriorityOfLoan]) VALUES (70, N'8796768', N'76876876', N'86', N'Less than  250,000', N'In a few weeks')
GO
INSERT [Users].[Borrower] ([ID], [CompanyID], [AmountTobeFunded], [DurationOfFunding], [Turnover], [PriorityOfLoan]) VALUES (71, N'8796768', N'76876876', N'86', N'Less than  250,000', N'In a few weeks')
GO
INSERT [Users].[Borrower] ([ID], [CompanyID], [AmountTobeFunded], [DurationOfFunding], [Turnover], [PriorityOfLoan]) VALUES (73, N'83209472', N'700', N'78', N'Less than  250,000', N'Urgent')
GO
INSERT [Users].[Borrower] ([ID], [CompanyID], [AmountTobeFunded], [DurationOfFunding], [Turnover], [PriorityOfLoan]) VALUES (74, N'83209472', N'700', N'12', N'Less than  250,000', N'In a few weeks')
GO
INSERT [Users].[Borrower] ([ID], [CompanyID], [AmountTobeFunded], [DurationOfFunding], [Turnover], [PriorityOfLoan]) VALUES (80, N'1', N'50000', N'12', N'More than  250,000', N'In a few weeks')
GO
INSERT [Users].[Borrower] ([ID], [CompanyID], [AmountTobeFunded], [DurationOfFunding], [Turnover], [PriorityOfLoan]) VALUES (81, N'1', N'30000', N'12', N'More than  250,000', N'In a few weeks')
GO
INSERT [Users].[Borrower] ([ID], [CompanyID], [AmountTobeFunded], [DurationOfFunding], [Turnover], [PriorityOfLoan]) VALUES (82, N'1', N'1500', N'12', N'More than  250,000', N'In a few weeks')
GO
INSERT [Users].[Lender] ([ID], [Sponsor], [CountryID]) VALUES (NULL, N'', 1)
GO
INSERT [Users].[Lender] ([ID], [Sponsor], [CountryID]) VALUES (NULL, N'', 1)
GO
INSERT [Users].[Lender] ([ID], [Sponsor], [CountryID]) VALUES (NULL, N'', 1)
GO
INSERT [Users].[Lender] ([ID], [Sponsor], [CountryID]) VALUES (42, N'ak340', 1)
GO
INSERT [Users].[Lender] ([ID], [Sponsor], [CountryID]) VALUES (49, N'Lorem', 1)
GO
INSERT [Users].[Lender] ([ID], [Sponsor], [CountryID]) VALUES (50, N'3480', 1)
GO
INSERT [Users].[Lender] ([ID], [Sponsor], [CountryID]) VALUES (51, N'7897', 1)
GO
INSERT [Users].[Lender] ([ID], [Sponsor], [CountryID]) VALUES (52, N'7879', 1)
GO
INSERT [Users].[Lender] ([ID], [Sponsor], [CountryID]) VALUES (54, N'2479', 1)
GO
INSERT [Users].[Lender] ([ID], [Sponsor], [CountryID]) VALUES (55, N'i324', 1)
GO
INSERT [Users].[Lender] ([ID], [Sponsor], [CountryID]) VALUES (56, N'489', 1)
GO
INSERT [Users].[Lender] ([ID], [Sponsor], [CountryID]) VALUES (72, N'348', 1)
GO
INSERT [Users].[Lender] ([ID], [Sponsor], [CountryID]) VALUES (57, N'489', 1)
GO
INSERT [Users].[Lender] ([ID], [Sponsor], [CountryID]) VALUES (58, N'768', 1)
GO
INSERT [Users].[Lender] ([ID], [Sponsor], [CountryID]) VALUES (79, N'12345', 1)
GO
SET IDENTITY_INSERT [Users].[Role] ON 
GO
INSERT [Users].[Role] ([ID], [Name]) VALUES (1, N'Admin')
GO
INSERT [Users].[Role] ([ID], [Name]) VALUES (2, N'Lender')
GO
INSERT [Users].[Role] ([ID], [Name]) VALUES (3, N'Borrower')
GO
SET IDENTITY_INSERT [Users].[Role] OFF
GO
SET IDENTITY_INSERT [Users].[User] ON 
GO
INSERT [Users].[User] ([ID], [RoleID], [Email], [Password], [FirstName], [LastName], [TimeStamp], [IsDeleted], [MobileNo], [Address], [ImagePath]) VALUES (78, 1, N'admin@admin.com', N'Zawar123', N'Zawar', N'Mahmood', CAST(N'2019-01-15T16:00:13.953' AS DateTime), 0, N'03074665233', NULL, NULL)
GO
INSERT [Users].[User] ([ID], [RoleID], [Email], [Password], [FirstName], [LastName], [TimeStamp], [IsDeleted], [MobileNo], [Address], [ImagePath]) VALUES (79, 2, N'lender@lender.com', N'Zawar123', N'Lender', N'Lender', CAST(N'2019-01-15T16:01:30.087' AS DateTime), 0, N'03074665233', NULL, NULL)
GO
INSERT [Users].[User] ([ID], [RoleID], [Email], [Password], [FirstName], [LastName], [TimeStamp], [IsDeleted], [MobileNo], [Address], [ImagePath]) VALUES (80, 3, N'borrower@borrower.com', N'Zawar123', N'Borrower', N'Borrower', CAST(N'2019-02-04T22:19:26.067' AS DateTime), 1, N'03074665233', NULL, NULL)
GO
INSERT [Users].[User] ([ID], [RoleID], [Email], [Password], [FirstName], [LastName], [TimeStamp], [IsDeleted], [MobileNo], [Address], [ImagePath]) VALUES (81, 3, N'borrower@borrower.com', N'Zawar123', N'Borrower', N'Borrower', CAST(N'2019-02-04T22:21:07.953' AS DateTime), 1, N'03074665233', NULL, NULL)
GO
INSERT [Users].[User] ([ID], [RoleID], [Email], [Password], [FirstName], [LastName], [TimeStamp], [IsDeleted], [MobileNo], [Address], [ImagePath]) VALUES (82, 3, N'borrower@borrower.com', N'Zawar123', N'Borrower', N'Borrower', CAST(N'2019-02-04T22:25:28.090' AS DateTime), 0, N'03074665233', NULL, NULL)
GO
SET IDENTITY_INSERT [Users].[User] OFF
GO
INSERT [Users].[UserAccount] ([UserID], [Amount]) VALUES (79, 10000)
GO
SET IDENTITY_INSERT [Users].[UserVerification] ON 
GO
INSERT [Users].[UserVerification] ([ID], [UserID], [EmailVerified], [PhoneVerified], [IdentityDocument], [IdentityDocumentVerified], [ProofOfResidency], [ProofOfResidencyVerified], [BankDocument], [BankDocumentVerified]) VALUES (21, 79, 1, 1, N'~/Images/VerificationImages/IdentityDocument_79.png', 1, N'~/Images/VerificationImages/ProofOfResidency.png', 1, N'~/Images/VerificationImages/BankStatement_79.jpg', 1)
GO
INSERT [Users].[UserVerification] ([ID], [UserID], [EmailVerified], [PhoneVerified], [IdentityDocument], [IdentityDocumentVerified], [ProofOfResidency], [ProofOfResidencyVerified], [BankDocument], [BankDocumentVerified]) VALUES (22, 80, 1, 1, N'~/Images/VerificationImages/IdentityDocument_80.pdf', 1, N'~/Images/VerificationImages/ProofOfResidency.pdf', 1, N'~/Images/VerificationImages/BankStatement_80.pdf', 1)
GO
INSERT [Users].[UserVerification] ([ID], [UserID], [EmailVerified], [PhoneVerified], [IdentityDocument], [IdentityDocumentVerified], [ProofOfResidency], [ProofOfResidencyVerified], [BankDocument], [BankDocumentVerified]) VALUES (23, 81, 1, 1, N'~/Images/VerificationImages/IdentityDocument_81.pdf', 1, N'~/Images/VerificationImages/ProofOfResidency.pdf', 1, N'~/Images/VerificationImages/BankStatement_81.pdf', 1)
GO
INSERT [Users].[UserVerification] ([ID], [UserID], [EmailVerified], [PhoneVerified], [IdentityDocument], [IdentityDocumentVerified], [ProofOfResidency], [ProofOfResidencyVerified], [BankDocument], [BankDocumentVerified]) VALUES (24, 82, 1, 1, N'~/Images/VerificationImages/IdentityDocument_82.pdf', 1, N'~/Images/VerificationImages/ProofOfResidency.pdf', 1, N'~/Images/VerificationImages/BankStatement_82.pdf', 1)
GO
SET IDENTITY_INSERT [Users].[UserVerification] OFF
GO
ALTER TABLE [Projects].[Project] ADD  CONSTRAINT [DF__Project__Feature__33D4B598]  DEFAULT ((0)) FOR [Featured]
GO
ALTER TABLE [Projects].[Project] ADD  CONSTRAINT [DF__Project__Approve__34C8D9D1]  DEFAULT ((0)) FOR [Approved]
GO
ALTER TABLE [Projects].[Project] ADD  CONSTRAINT [DF__Project__TimeLef__4D94879B]  DEFAULT (getdate()) FOR [TimeLeft]
GO
ALTER TABLE [Projects].[Project] ADD  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [Projects].[Project] ADD  DEFAULT ((0)) FOR [Popularity]
GO
ALTER TABLE [Projects].[Project_Transactions] ADD  DEFAULT (getdate()) FOR [TimeStamp]
GO
ALTER TABLE [Users].[ActivationCode] ADD  DEFAULT (dateadd(day,(2),[dbo].[DEFAULTDATE]())) FOR [Expires]
GO
ALTER TABLE [Users].[ActivationCode] ADD  DEFAULT ([dbo].[DEFAULTDATE]()) FOR [TimeStamp]
GO
ALTER TABLE [Users].[User] ADD  CONSTRAINT [DF_Users_TimeStamp]  DEFAULT ([dbo].[DefaultDate]()) FOR [TimeStamp]
GO
ALTER TABLE [Users].[User] ADD  CONSTRAINT [DF_Users_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [Users].[UserAccount] ADD  DEFAULT ((0)) FOR [Amount]
GO
ALTER TABLE [Users].[UserVerification] ADD  DEFAULT ((0)) FOR [EmailVerified]
GO
ALTER TABLE [Users].[UserVerification] ADD  DEFAULT ((0)) FOR [PhoneVerified]
GO
ALTER TABLE [Users].[UserVerification] ADD  DEFAULT ((0)) FOR [IdentityDocumentVerified]
GO
ALTER TABLE [Users].[UserVerification] ADD  DEFAULT ((0)) FOR [ProofOfResidencyVerified]
GO
ALTER TABLE [Users].[UserVerification] ADD  DEFAULT ((0)) FOR [BankDocumentVerified]
GO
ALTER TABLE [Users].[ActivationCode]  WITH CHECK ADD FOREIGN KEY([UserID])
REFERENCES [Users].[User] ([ID])
GO
ALTER TABLE [Users].[Lender]  WITH CHECK ADD  CONSTRAINT [FK__Lenders__Country__2D27B809] FOREIGN KEY([CountryID])
REFERENCES [Others].[Country] ([ID])
GO
ALTER TABLE [Users].[Lender] CHECK CONSTRAINT [FK__Lenders__Country__2D27B809]
GO
ALTER TABLE [Users].[UserVerification]  WITH CHECK ADD FOREIGN KEY([UserID])
REFERENCES [Users].[User] ([ID])
GO
/****** Object:  StoredProcedure [dbo].[RegisterBorrower]    Script Date: 2/10/2019 9:41:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Zawar
-- Create date: 12.31.18
-- Description:	Proc to Register Borrower
-- =============================================
CREATE PROCEDURE [dbo].[RegisterBorrower]
@FirstName VARCHAR(50), 
@LastName VARCHAR(50), 
@Email VARCHAR(255), 
@Password VARCHAR(50),
@MobileNo VARCHAR(50),
@CompanyID INT,
@AmountToBeFunded INT,
@DurationOfFunding INT,
@TurnOver VARCHAR(50), 
@PriorityOfLoan VARCHAR(50),
@EmailCode VARCHAR(4),
@PhoneCode VARCHAR(4)
AS
BEGIN
SET NOCOUNT ON;
BEGIN TRY
BEGIN TRANSACTION

INSERT INTO [Users].[User]([FirstName] , [LastName] , [Email] , [Password], [MobileNo] , [RoleID]  )  
VALUES (@Firstname,@LastName, @Email, @Password, @MobileNo, dbo.LenderID());
DECLARE @ID INT = SCOPE_IDENTITY(); 
INSERT INTO [Users].[Borrower] 
(ID, CompanyID, AmountTobeFunded, DurationOfFunding, Turnover, PriorityOfLoan) 
VALUES (@ID, @CompanyID,@AmountToBeFunded, @DurationOfFunding, @Turnover, @PriorityOfLoan); 
INSERT INTO [Users].[ActivationCode] ([Code] ,[UserID] ,[Type]) VALUES (@EmailCode ,@ID ,'Email');
INSERT INTO [Users].[ActivationCode] ([Code] ,[UserID] ,[Type]) VALUES (@PhoneCode ,@ID ,'Phone');
INSERT INTO [Users].[UserVerification] 
(UserID, IdentityDocument, ProofOfResidency, BankDocument)
VALUES (@ID ,null,null,null  )
SELECT @ID
COMMIT 
                END TRY 
                BEGIN CATCH 
                declare @ErrorMessage nvarchar(max), @ErrorSeverity int, @ErrorState int;
                select @ErrorMessage = ERROR_MESSAGE() + ' Line ' + cast(ERROR_LINE() as nvarchar(5)), @ErrorSeverity = ERROR_SEVERITY(), @ErrorState = ERROR_STATE();
                rollback transaction;
                raiserror(@ErrorMessage, @ErrorSeverity, @ErrorState);
                END CATCH

END
GO
/****** Object:  StoredProcedure [dbo].[RegisterLender]    Script Date: 2/10/2019 9:41:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Zawar
-- Create date: 12.31.18
-- Description:	Proc to Register Lender
-- =============================================
CREATE PROCEDURE [dbo].[RegisterLender]
@FirstName VARCHAR(50), 
@LastName VARCHAR(50), 
@Email VARCHAR(255), 
@Password VARCHAR(50),
@MobileNo VARCHAR(50),
@CountryID INT,
@Sponsor VARCHAR(MAX),
@EmailCode VARCHAR(4),
@PhoneCode VARCHAR(4)
AS
BEGIN
SET NOCOUNT ON;
BEGIN TRY
BEGIN TRANSACTION

INSERT INTO [Users].[User]([FirstName] , [LastName] , [Email] , [Password], [MobileNo] , [RoleID]  )  
VALUES (@Firstname,@LastName, @Email, @Password, @MobileNo, dbo.LenderID());
DECLARE @ID INT = SCOPE_IDENTITY(); 
INSERT INTO [Users].[Lender] (ID, CountryID, Sponsor) VALUES (@ID,@CountryID,@Sponsor); 
INSERT INTO [Users].[ActivationCode] ([Code] ,[UserID] ,[Type]) VALUES (@EmailCode ,@ID ,'Email');
INSERT INTO [Users].[ActivationCode] ([Code] ,[UserID] ,[Type]) VALUES (@PhoneCode ,@ID ,'Phone');
INSERT INTO [Users].[UserVerification] 
(UserID, IdentityDocument, ProofOfResidency, BankDocument)
VALUES (@ID ,null,null,null  )
SELECT @ID
COMMIT 
                END TRY 
                BEGIN CATCH 
                declare @ErrorMessage nvarchar(max), @ErrorSeverity int, @ErrorState int;
                select @ErrorMessage = ERROR_MESSAGE() + ' Line ' + cast(ERROR_LINE() as nvarchar(5)), @ErrorSeverity = ERROR_SEVERITY(), @ErrorState = ERROR_STATE();
                rollback transaction;
                raiserror(@ErrorMessage, @ErrorSeverity, @ErrorState);
                END CATCH

END
GO
USE [master]
GO
ALTER DATABASE [CrowdFunding] SET  READ_WRITE 
GO
